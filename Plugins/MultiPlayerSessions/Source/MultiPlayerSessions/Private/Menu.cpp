// Fill out your copyright notice in the Description page of Project Settings.


#include "Menu.h"
#include "OnlineSubsystem.h"


void UMenu::MenuSetup(int32 InNumPublicConnections, FString InMatchType, FString LobbyPath)
{
	if (GEngine) {
		GEngine->AddOnScreenDebugMessage(
			-1,
			10,
			FColor::Green,
			FString::Printf(TEXT("SetupSetting : MaxPlayers=%d, MatchType=%s, LobbyPath=%s"), InNumPublicConnections, *InMatchType, *LobbyPath)
			);
	}
	// init our Menu, set the visibility;
	PathToLobby = FString::Printf(TEXT("%s?listen"), *LobbyPath);
	AddToViewport();
	SetVisibility(ESlateVisibility::Visible);
	this->NumPublicConnections = InNumPublicConnections;
	this->MatchType = InMatchType;
	
	/** Setting this flag to true, allows this widget to accept focus when clicked, or when navigated to. */
	bIsFocusable = true;

	UWorld* World = GetWorld();
	if (World != nullptr) {
		APlayerController* PlayerController = World->GetFirstPlayerController();
		if (IsValid(PlayerController)) {
			// Set the Mouse input mode;
			FInputModeUIOnly InputModeData;
			InputModeData.SetWidgetToFocus(TakeWidget());
			// Can move Mouse curse freedom;
			InputModeData.SetLockMouseToViewportBehavior(EMouseLockMode::DoNotLock);
			PlayerController->SetInputMode(InputModeData);
			PlayerController->bShowMouseCursor = true;
		}
	}

	// init the Pointer to Our SessionSubsystem; then we can use it's interface;
	UGameInstance* GameInstance = GetGameInstance();
	if (GameInstance) {
		MultiplayerSessionSubsystem = GameInstance->GetSubsystem<UMultiplayerSessionSubsystem>();

		// Bind our callback OnCreateSession Function
		if (MultiplayerSessionSubsystem) {
			MultiplayerSessionSubsystem->MultiPlayerOnCreateSessionComplete.AddDynamic(this, &ThisClass::OnCreateSession);
			MultiplayerSessionSubsystem->MultiPlayerOnDestroySessionComplete.AddDynamic(this, &ThisClass::OnDestroySession);
			MultiplayerSessionSubsystem->MultiPlayerOnStartSessionComplete.AddDynamic(this, &ThisClass::OnStartSession);
			MultiplayerSessionSubsystem->MultiPlayerOnFindSessionComplete.AddUObject(this, &ThisClass::OnFindSession);
			MultiplayerSessionSubsystem->MultiPlayerOnJoinSessionComplete.AddUObject(this, &ThisClass::OnJoinSession);
		}
	}
}

bool UMenu::Initialize()
{
	if (! Super::Initialize()) {
		return false;
	}

	// Bind Our Callback function
	if (HostButton) {
		HostButton->OnClicked.AddDynamic(this, &ThisClass::HostButtonClicked);
	}

	if (JoinButton) {
		JoinButton->OnClicked.AddDynamic(this, &ThisClass::JoinButtonClicked);
	}

	return true;
}

void UMenu::OnLevelRemovedFromWorld(ULevel* InLevel, UWorld* InWorld)
{
	MenuTearDown();
	
	Super::OnLevelRemovedFromWorld(InLevel, InWorld);
}

void UMenu::SetShooterName(FString Name)
{
	this->ShooterName = Name;
}
void UMenu::SetMatchType(FString InMatchType)
{
	this->MatchType = InMatchType;
}

void UMenu::SetMaxConnections(int32 MaxCount)
{
	this->NumPublicConnections = MaxCount;
}

void UMenu::HostButtonClicked()
{
	HostButton->SetIsEnabled(false);
	if (IsValid(MultiplayerSessionSubsystem)) {
		MultiplayerSessionSubsystem->CreateSession(NumPublicConnections, MatchType);
	}
}

void UMenu::JoinButtonClicked()
{
	JoinButton->SetIsEnabled(false);
	if (IsValid(MultiplayerSessionSubsystem)) {
		MultiplayerSessionSubsystem->FindSessions(10000);
	}
	
}


void UMenu::MenuTearDown()
{
	
	// we should set the mouse mode the game mode;
	APlayerController* PlayerController = GetWorld()->GetFirstPlayerController();
	FInputModeGameOnly InputModeGameOnly;
	
	if (PlayerController) {
		PlayerController->SetInputMode(InputModeGameOnly);
		PlayerController->SetShowMouseCursor(false);
	}
}

void UMenu::OnCreateSession(bool bSuccessful)
{
	
	if (GEngine) {
		GEngine->AddOnScreenDebugMessage(
			-1,
			10,
			FColor::Cyan,
			FString::Printf(TEXT("CreateSession Result = %d,MaxPlayers=%d, CODE=%s, Path=%s"), bSuccessful , NumPublicConnections, *MatchType, *PathToLobby)
			);
	}
	if (bSuccessful) {
		// if create session successful, then we travel to the lobby;
		UWorld* World = GetWorld();
		if (World) {
			bool result = World->ServerTravel(PathToLobby);
			if (GEngine) {
			GEngine->AddOnScreenDebugMessage(
			-1,
			10,
			FColor::Red,
			FString::Printf(TEXT("Travel Result = %d"), result)
			);
			}
		}
	} else {
		HostButton->SetIsEnabled(true);
	}
}

void UMenu::OnStartSession(bool bSuccessful)
{
	if (GEngine) {
		GEngine->AddOnScreenDebugMessage(
			-1,
			10,
			FColor::Green,
			FString::Printf(TEXT("StartSession Result = %d"), bSuccessful ) 
			);
	}
}

void UMenu::OnDestroySession(bool bSuccessful) {}

void UMenu::OnFindSession(const TArray<FOnlineSessionSearchResult>& SessionResults, bool bSuccessful)
{
	if (GEngine) {
		GEngine->AddOnScreenDebugMessage(
			-1,
			10,
			FColor::Red,
			FString::Printf(TEXT("Find Session Result = %d, Room Number= %d"), bSuccessful, SessionResults.Num())
		);
	}
	if (bSuccessful == false || SessionResults.Num() <= 0) {
		JoinButton->SetIsEnabled(true);
	}
	
	if (!IsValid(MultiplayerSessionSubsystem)) {
		return;
	}
	// when find session Complete then chose correct session to join in;
	for (auto Result :SessionResults) {
		FString SettingValue;
		Result.Session.SessionSettings.Get(FName(("MatchType")), SettingValue);
		// if we search the match type, then join in, and return;
		if (SettingValue == MatchType) {

			if (GEngine) {
				GEngine->AddOnScreenDebugMessage(
					-1,
					10,
					FColor::Yellow,
					FString("Find Session Successful,then join it")
				);
			}
			MultiplayerSessionSubsystem->JoinSession(Result);
			return;
		}
	}
}

void UMenu::OnJoinSession(EOnJoinSessionCompleteResult::Type Result)
{
	if (Result == EOnJoinSessionCompleteResult::UnknownError) {
		return;;
	}
	
	IOnlineSubsystem* Subsystem = IOnlineSubsystem::Get();
	if (Subsystem) {
		IOnlineSessionPtr OnlineSessionInterface = Subsystem->GetSessionInterface();
		if (OnlineSessionInterface.IsValid()) {
			FString Address;
			//* Returns the platform specific connection information for joining the match.
			OnlineSessionInterface->GetResolvedConnectString(NAME_GameSession, Address);

			APlayerController* PlayerController = GetGameInstance()->GetFirstLocalPlayerController();
			if (PlayerController) {
				PlayerController->ClientTravel(Address, ETravelType::TRAVEL_Absolute);
			}
		}
	}
}
