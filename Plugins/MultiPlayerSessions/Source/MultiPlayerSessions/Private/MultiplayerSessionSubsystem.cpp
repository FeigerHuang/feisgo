// Fill out your copyright notice in the Description page of Project Settings.


#include "MultiplayerSessionSubsystem.h"
#include "OnlineSubsystem.h"

UMultiplayerSessionSubsystem::UMultiplayerSessionSubsystem()
	:CreateSessionCompleteDelegate(FOnCreateSessionCompleteDelegate::CreateUObject(this, &ThisClass::OnCreateSessionComplete)),
	FindSessionsCompleteDelegate(FOnFindSessionsCompleteDelegate::CreateUObject(this, &ThisClass::OnFindSessionsComplete)),
	JoinSessionCompleteDelegate(FOnJoinSessionCompleteDelegate::CreateUObject(this, &ThisClass::OnJoinSessionComplete)),
	DestroySessionCompleteDelegate(FOnDestroySessionCompleteDelegate::CreateUObject(this, &ThisClass::OnDestroySessionComplete)),
	StartSessionCompleteDelegate(FOnStartSessionCompleteDelegate::CreateUObject(this, &ThisClass::OnStartSessionComplete))

{
	// 初始化 SessionInterface 对象;
	IOnlineSubsystem* OnlineSubsystem = IOnlineSubsystem::Get();
	if (OnlineSubsystem != nullptr) {
		SessionInterface = OnlineSubsystem->GetSessionInterface();
	}
}

void UMultiplayerSessionSubsystem::CreateSession(int32 NumPublicConnections, FString MatchType)
{
	if (SessionInterface == nullptr) {
		return;
	}

	// if exit session , free the old session
	auto ExistSession = SessionInterface->GetNamedSession(NAME_GameSession);
	if (ExistSession) {
		bCreateSessionOnDestroy = true;
		LastNumberPublicConnections = NumPublicConnections;
		LastMatchType = MatchType;
		// DestroySession is asynchronous, will send to server;
		DestroySession();
	}

	// before we Create the session, register the delegate;
	CreateSessionCompleteDelegate_Handle =
		SessionInterface->AddOnCreateSessionCompleteDelegate_Handle(CreateSessionCompleteDelegate);
	
	// then we should init setting, and create Session
	LastSessionSets = MakeShareable(new FOnlineSessionSettings);
	// if Subsystem don't exit, then we use local Area network;
	LastSessionSets->bIsLANMatch = IOnlineSubsystem::Get()->GetSubsystemName() == "NULL" ? true : false;
	LastSessionSets->NumPublicConnections = NumPublicConnections;
	LastSessionSets->bAllowJoinInProgress = true;
	LastSessionSets->bAllowJoinViaPresence = true;
	LastSessionSets->bShouldAdvertise = true;
	LastSessionSets->bUsesPresence = true;
	LastSessionSets->BuildUniqueId = 1;
	
	LastSessionSets->Set("MatchType", MatchType, EOnlineDataAdvertisementType::ViaOnlineServiceAndPing);
	ULocalPlayer* LocalPlayer = GetWorld()->GetFirstLocalPlayerFromController();
	bool bSuccessful = SessionInterface->CreateSession(*LocalPlayer->GetPreferredUniqueNetId(), NAME_GameSession, *LastSessionSets);
	// if we create the session failed, the we unregister our deleagate
	if (!bSuccessful) {
		SessionInterface->ClearOnCreateSessionCompleteDelegate_Handle(CreateSessionCompleteDelegate_Handle);
	}

	if (MultiPlayerOnCreateSessionComplete.IsBound()) {
		MultiPlayerOnCreateSessionComplete.Broadcast(bSuccessful);
	}
}

void UMultiplayerSessionSubsystem::FindSessions(int32 MaxSearchResults)
{
	if (SessionInterface == nullptr) {
		MultiPlayerOnFindSessionComplete.Broadcast(TArray<FOnlineSessionSearchResult>(), false);
		return ;
	}
	
	// record handle, and add delegate in list; when findSession complete call back;
	FindSessionCompleteDelegate_Handle = SessionInterface->AddOnFindSessionsCompleteDelegate_Handle(FindSessionsCompleteDelegate);

	LastSessionSearch = MakeShareable(new FOnlineSessionSearch);
	LastSessionSearch->MaxSearchResults = MaxSearchResults;
	LastSessionSearch->bIsLanQuery = IOnlineSubsystem::Get()->GetSubsystemName() == "NULL" ? true : false;
	LastSessionSearch->QuerySettings.Set(SEARCH_PRESENCE, true, EOnlineComparisonOp::Equals);

	const ULocalPlayer* LocalPlayer = GetWorld()->GetFirstLocalPlayerFromController();
	
	bool bFound = SessionInterface->FindSessions(*LocalPlayer->GetPreferredUniqueNetId(), LastSessionSearch.ToSharedRef());
	// if we don't find result, return empty search result;

	if (GEngine) {
		GEngine->AddOnScreenDebugMessage(-1,10, FColor::Magenta, FString::Printf(TEXT("FindSession Result=%d"), bFound));
	}
	
	if (!bFound) {
		MultiPlayerOnFindSessionComplete.Broadcast(TArray<FOnlineSessionSearchResult>(), false);
		SessionInterface->ClearOnFindSessionsCompleteDelegate_Handle(FindSessionCompleteDelegate_Handle);	
	}
	
}


void UMultiplayerSessionSubsystem::JoinSession(const FOnlineSessionSearchResult& SessionResult)
{
	if (!SessionInterface.IsValid()) {
		MultiPlayerOnJoinSessionComplete.Broadcast(EOnJoinSessionCompleteResult::UnknownError);
		return;
	}
	
	JoinSessionCompleteDelegate_Handle = SessionInterface->AddOnJoinSessionCompleteDelegate_Handle(JoinSessionCompleteDelegate);
	const ULocalPlayer* LocalPlayer = GetWorld()->GetFirstLocalPlayerFromController();
	// if join session failed broadcast failed;
	if (!SessionInterface->JoinSession(*LocalPlayer->GetPreferredUniqueNetId(), NAME_GameSession, SessionResult)) {
		SessionInterface->ClearOnJoinSessionCompleteDelegate_Handle(JoinSessionCompleteDelegate_Handle);
		MultiPlayerOnJoinSessionComplete.Broadcast(EOnJoinSessionCompleteResult::UnknownError);
	}
}

void UMultiplayerSessionSubsystem::DestroySession()
{
	if (!SessionInterface.IsValid()) {
		MultiPlayerOnDestroySessionComplete.Broadcast(false);
		return;
	}

	DestroySessionCompleteDelegate_Handle =
		SessionInterface->AddOnDestroySessionCompleteDelegate_Handle(DestroySessionCompleteDelegate);
	// OnDestroy Failed then Broadcast;
	if (!SessionInterface->DestroySession(NAME_GameSession)) {
		SessionInterface->ClearOnDestroySessionCompleteDelegate_Handle(DestroySessionCompleteDelegate_Handle);
		MultiPlayerOnDestroySessionComplete.Broadcast(false);
	}
	// if destroy Success then callback the OnDestroySessionComplete
}

void UMultiplayerSessionSubsystem::StartSession()
{
	StartSessionCompleteDelegate_Handle = SessionInterface->AddOnStartSessionCompleteDelegate_Handle(StartSessionCompleteDelegate);
	
	if (!SessionInterface->StartSession(NAME_GameSession)) {
		MultiPlayerOnStartSessionComplete.Broadcast(false);
		// if we start session failed then clear the start session list;
		SessionInterface->ClearOnStartSessionCompleteDelegate_Handle(StartSessionCompleteDelegate_Handle);
	}
}

void UMultiplayerSessionSubsystem::OnCreateSessionComplete(FName SessionName, bool bSuccessful)
{
	// if we create Session Complete, clear the Delegate list;
	if (SessionInterface) {
		SessionInterface->ClearOnCreateSessionCompleteDelegate_Handle(CreateSessionCompleteDelegate_Handle);
	}
}

// when the find Session completed, we need call back then bind function;
void UMultiplayerSessionSubsystem::OnFindSessionsComplete(bool bSuccessful)
{
	if (!bSuccessful || LastSessionSearch->SearchResults.Num() <= 0) {
		MultiPlayerOnFindSessionComplete.Broadcast(TArray<FOnlineSessionSearchResult>(), false);
		return;
	}

	MultiPlayerOnFindSessionComplete.Broadcast(LastSessionSearch->SearchResults, true);
}

void UMultiplayerSessionSubsystem::OnJoinSessionComplete(FName SessionName, EOnJoinSessionCompleteResult::Type JoinResult) {
	if (SessionInterface) {
		SessionInterface->ClearOnJoinSessionCompleteDelegate_Handle(JoinSessionCompleteDelegate_Handle);
	}

	MultiPlayerOnJoinSessionComplete.Broadcast(JoinResult);
}

void UMultiplayerSessionSubsystem::OnDestroySessionComplete(FName SessionName, bool bSuccessful)
{
	if (SessionInterface) {
		SessionInterface->ClearOnDestroySessionCompleteDelegate_Handle(DestroySessionCompleteDelegate_Handle);
	}
	// if need create session, call create session
	if (bSuccessful && bCreateSessionOnDestroy) {
		bCreateSessionOnDestroy = false;
		CreateSession(LastNumberPublicConnections, LastMatchType);
	}
	MultiPlayerOnDestroySessionComplete.Broadcast(bSuccessful);
}

void UMultiplayerSessionSubsystem::OnStartSessionComplete(FName SessionName, bool bSuccessful)
{
	if (SessionInterface) {
		SessionInterface->ClearOnStartSessionCompleteDelegate_Handle(StartSessionCompleteDelegate_Handle);
	}

	MultiPlayerOnStartSessionComplete.Broadcast(bSuccessful);
}
