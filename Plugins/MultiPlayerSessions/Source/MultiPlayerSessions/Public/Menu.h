// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "MultiplayerSessionSubsystem.h"
#include "Blueprint/UserWidget.h"
#include "Components/Button.h"
#include "Menu.generated.h"

/**
 * 
 */
UCLASS()
class MULTIPLAYERSESSIONS_API UMenu : public UUserWidget
{
	GENERATED_BODY()
public:
	UFUNCTION(BlueprintCallable, Category = "Menu")
	void MenuSetup(int32 InNumPublicConnections = 5, FString InMatchType = "FreeForAll", FString LobbyPath = FString(""));
	
	virtual bool Initialize() override;

	virtual void OnLevelRemovedFromWorld(ULevel* InLevel, UWorld* InWorld) override;

	UFUNCTION()
	void HostButtonClicked();

	UFUNCTION()
	void JoinButtonClicked();

	UFUNCTION(BlueprintCallable)
	void SetShooterName(FString Name);

	UFUNCTION(BlueprintCallable)
	void SetMatchType(FString InMatchType);

	UFUNCTION(BlueprintCallable)
	void SetMaxConnections(int32 MaxCount);
protected:
	UPROPERTY(meta=(BindWidget))
	UButton* HostButton;

	UPROPERTY(meta=(BindWidget))
	UButton* JoinButton;

	void MenuTearDown();

	// the Call Back function will call back when event happen;
	UFUNCTION()
	void OnCreateSession(bool bSuccessful);
	UFUNCTION()
	void OnStartSession(bool bSuccessful);
	UFUNCTION()
	void OnDestroySession(bool bSuccessful);

	void OnFindSession(const TArray<FOnlineSessionSearchResult>& SessionResults, bool bSuccessful);

	void OnJoinSession(EOnJoinSessionCompleteResult::Type Result);
private:
	UMultiplayerSessionSubsystem* MultiplayerSessionSubsystem;

	int32 NumPublicConnections = 5;

	UPROPERTY(BlueprintReadOnly, meta=(AllowPrivateAccess=true))
	FString MatchType = FString(TEXT("FreeForAll"));

	FString PathToLobby = FString(TEXT("Game/Maps/ShooterLobbyMap?listen"));

	UPROPERTY(BlueprintReadOnly, meta=(AllowPrivateAccess=true))
	FString ShooterName;
};
