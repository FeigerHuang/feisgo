// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "OnlineSessionSettings.h"
#include "Interfaces/OnlineSessionInterface.h"
#include "Subsystems/GameInstanceSubsystem.h"
#include "MultiplayerSessionSubsystem.generated.h"



/**
 *	Custom can  register this Delegate when SessionCreate Finish; 
 */
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FMultiPlayerOnCreateSessionDelegate, bool, bWasSuccessful);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FMultiPlayerOnStartSessionDelegate, bool, bWasSuccessful);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FMultiPlayerOnDestroySessionDelegate, bool, bWasSuccessful);
DECLARE_MULTICAST_DELEGATE_TwoParams(FMultiPlayerOnFindSessionDelegate, const TArray<FOnlineSessionSearchResult>& SessionResults, bool bWasSuccessful);
DECLARE_MULTICAST_DELEGATE_OneParam(FMultiPlayerOnJoinSessionDelegate, EOnJoinSessionCompleteResult::Type Result);

UCLASS()
class MULTIPLAYERSESSIONS_API UMultiplayerSessionSubsystem : public UGameInstanceSubsystem
{
	GENERATED_BODY()
public:
	UMultiplayerSessionSubsystem();

	/*
	 * To handle session functionality. the Menu class will call these
	 */

	void CreateSession(int32 NumPublicConnections, FString MatchType);
	void FindSessions(int32 MaxSearchResults);
	void JoinSession(const FOnlineSessionSearchResult& SessionResult);
	void DestroySession();
	void StartSession();

	// When we Create Session, transmit the result, Custom use these Delegates to bind call back;
	FMultiPlayerOnCreateSessionDelegate MultiPlayerOnCreateSessionComplete;
	FMultiPlayerOnFindSessionDelegate MultiPlayerOnFindSessionComplete;
	FMultiPlayerOnJoinSessionDelegate MultiPlayerOnJoinSessionComplete;
	FMultiPlayerOnStartSessionDelegate MultiPlayerOnStartSessionComplete;
	FMultiPlayerOnDestroySessionDelegate MultiPlayerOnDestroySessionComplete;
protected:
	
	// Internal Callbacks for the Delegates we'll add to the online Session Interface delegate list
	// This don't need to be called outside the class.
	void OnCreateSessionComplete(FName SessionName, bool bSuccessful);
	void OnFindSessionsComplete(bool bSuccessful);
	void OnJoinSessionComplete(FName SessionName, EOnJoinSessionCompleteResult::Type JoinResult);
	void OnDestroySessionComplete(FName SessionName, bool bSuccessful);
	void OnStartSessionComplete(FName SessionName, bool bSuccessful);
private:
	IOnlineSessionPtr SessionInterface;

	TSharedPtr<FOnlineSessionSettings>  LastSessionSets;
	TSharedPtr<FOnlineSessionSearch> LastSessionSearch;
	/*
	 * To add to the Online Session Interface delegate list.
	 * We'll bind our MultiplayerSessionSubsystem internal callbacks to these;
	 */
	FOnCreateSessionCompleteDelegate CreateSessionCompleteDelegate;
	FDelegateHandle CreateSessionCompleteDelegate_Handle;
	
	FOnFindSessionsCompleteDelegate FindSessionsCompleteDelegate;
	FDelegateHandle FindSessionCompleteDelegate_Handle;
	
	FOnJoinSessionCompleteDelegate JoinSessionCompleteDelegate;
	FDelegateHandle JoinSessionCompleteDelegate_Handle;
	
	FOnDestroySessionCompleteDelegate DestroySessionCompleteDelegate;
	FDelegateHandle DestroySessionCompleteDelegate_Handle;
	
	FOnStartSessionCompleteDelegate StartSessionCompleteDelegate;
	FDelegateHandle StartSessionCompleteDelegate_Handle;

	bool bCreateSessionOnDestroy{false};
	int32 LastNumberPublicConnections;
	FString LastMatchType;
};
