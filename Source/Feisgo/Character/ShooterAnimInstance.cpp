// Fill out your copyright notice in the Description page of Project Settings.


#include "ShooterAnimInstance.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Kismet/KismetMathLibrary.h"

void UShooterAnimInstance::NativeBeginPlay()
{
	Super::NativeBeginPlay();

	InitCharacter();
}

void UShooterAnimInstance::NativeUpdateAnimation(float DeltaSeconds)
{
	Super::NativeUpdateAnimation(DeltaSeconds);

	if (Character == nullptr) {
		InitCharacter();
	}
	
	if (Character) {
		FVector Velocity = Character->GetVelocity();
		Velocity.Z = 0.f;
		Speed = Velocity.Size();

		bInAir = Character->GetCharacterMovement()->IsFalling();

		bAccelerate = (Character->GetCharacterMovement()->GetCurrentAcceleration().IsNearlyZero(0.1)) ? false : true;

		bEquipped = Character->IsEquipped();
		bIsCrouch = Character->bIsCrouched;
        bEliminated = Character->GetEliminated();
		bIsAimming = Character->IsAimming();
		bSprint = Character->IsSprint();
		// offset Yaw for Strafing
		FRotator WorldRotation = Character->GetBaseAimRotation();
		FRotator MovementRotation = UKismetMathLibrary::MakeRotFromX(Character->GetVelocity());
		FRotator TargetRotation = UKismetMathLibrary::NormalizedDeltaRotator(MovementRotation, WorldRotation);
		DeltaRotation = FMath::RInterpTo(DeltaRotation, TargetRotation, DeltaSeconds, 6.f);
		YawOffset = DeltaRotation.Yaw;
		
		// Calculate the DeltaRot between these Rotation;
		CharacterRotLastFrame = CharacterRotation;
		CharacterRotation = Character->GetActorRotation();
		FRotator DeltaRot = UKismetMathLibrary::NormalizedDeltaRotator(CharacterRotation, CharacterRotLastFrame);
		const float Target = DeltaRot.Yaw / DeltaSeconds;
		Lean = FMath::FInterpTo(Lean, Target, DeltaSeconds, 5.f);
		Lean = FMath::Clamp(Lean, -90.f, 90.f);

		AO_Yaw = Character->GetAimOffsetsYaw();
		AO_Pitch = Character->GetAimOffsetsPitch();
		TurnState = Character->GetTurnState();
		
		const AWeapon* Weapon = Character->GetEquippedWeapon();
		if (Weapon && Weapon->GetSkeletalMesh()) {
			auto WeaponMesh = Weapon->GetSkeletalMesh();
			FTransform WeaponSocketTrans = WeaponMesh->GetSocketTransform(FName("WeaponHandSocket"), ERelativeTransformSpace::RTS_World);
			auto ShooterMesh = Character->GetMesh();
			if (ShooterMesh) {
				FVector Location;
				FRotator Rotation;
				// trans the WorkSpace to Bone Hand_r space;
				// 我们要调整的是Left_hand, 使用FABRIK时, Effector Target是hand_r, 根据hand_l相对hand_r的坐标调整;
				ShooterMesh->TransformToBoneSpace(
					FName("hand_r"),
					WeaponSocketTrans.GetLocation(),
					FRotator::ZeroRotator,
					Location,
					Rotation
				);
				WeaponHandTransform.SetLocation(Location);
				WeaponHandTransform.SetRotation(FQuat(Rotation));
			}
		}
		
		
	}
}

