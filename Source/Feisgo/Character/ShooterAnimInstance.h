// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "ShooterCharacter.h"
#include "Animation/AnimInstance.h"
#include "ShooterAnimInstance.generated.h"

/**
 * 
 */
UCLASS()
class FEISGO_API UShooterAnimInstance : public UAnimInstance
{
	GENERATED_BODY()

public:
	virtual void NativeBeginPlay() override;

	virtual void NativeUpdateAnimation(float DeltaSeconds) override;

protected:
	FORCEINLINE void InitCharacter() {Character = Cast<AShooterCharacter>(TryGetPawnOwner());}

private:

	UPROPERTY(BlueprintReadOnly, Category = "Character", meta=(AllowPrivateAccess=true))
	AShooterCharacter* Character;

	UPROPERTY(BlueprintReadOnly, Category = "Character", meta=(AllowPrivateAccess=true))
	float Speed;	

	UPROPERTY(BlueprintReadOnly, Category = "Character", meta=(AllowPrivateAccess=true))
	bool bInAir;

	UPROPERTY(BlueprintReadOnly, Category = "Character", meta=(AllowPrivateAccess=true))
	bool bAccelerate;

	UPROPERTY(BlueprintReadOnly, Category = "Character", meta=(AllowPrivateAccess=true))
	bool bEquipped;

	UPROPERTY(BlueprintReadOnly, Category = "Character", meta=(AllowPrivateAccess=true))
	bool bIsCrouch;

	UPROPERTY(BlueprintReadOnly, Category = "Character", meta=(AllowPrivateAccess=true))
	bool bIsAimming;

	UPROPERTY(BlueprintReadOnly, Category = "Character", meta=(AllowPrivateAccess=true))
	float YawOffset;

	UPROPERTY(BlueprintReadOnly, Category = "Character", meta=(AllowPrivateAccess=true))
	float Lean;

	UPROPERTY(BlueprintReadOnly, Category = "Character", meta=(AllowPrivateAccess=true))
	float AO_Yaw;

	UPROPERTY(BlueprintReadOnly, Category = "Character", meta=(AllowPrivateAccess=true))
	float AO_Pitch;

	UPROPERTY(BlueprintReadOnly, Category = "Character", meta=(AllowPrivateAccess=true))
	FTransform WeaponHandTransform;

	UPROPERTY(BlueprintReadOnly, Category = "Character", meta=(AllowPrivateAccess=true))
	ETurnInPlace TurnState;
	
	UPROPERTY(BlueprintReadOnly, Category = "Character", meta=(AllowPrivateAccess=true))
	bool bEliminated;

	UPROPERTY(BlueprintReadOnly, Category = "Character", meta=(AllowPrivateAccess=true))
	bool bSprint;

	FRotator CharacterRotLastFrame;
	FRotator CharacterRotation;
	FRotator DeltaRotation;
};
