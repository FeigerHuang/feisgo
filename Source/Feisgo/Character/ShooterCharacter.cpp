// Fill out your copyright notice in the Description page of Project Settings.


#include "ShooterCharacter.h"
#include "ShooterAnimInstance.h"
#include "Feisgo/GamePlay/Weapon.h"
#include "Components/CapsuleComponent.h"
#include "Feisgo/Feisgo.h"
#include "Feisgo/Framework/ShooterGameMode.h"
#include "Feisgo/ShooterComponent/CombatComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetMathLibrary.h"
#include "Net/UnrealNetwork.h"


// Sets default values
AShooterCharacter::AShooterCharacter()
{
	PrimaryActorTick.bCanEverTick = true;
	bReplicates = true;
	SpawnCollisionHandlingMethod = ESpawnActorCollisionHandlingMethod::AdjustIfPossibleButAlwaysSpawn;
	
	// spring arm use the Controller's rotation;
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(GetMesh());
	CameraBoom->TargetArmLength = this->TargetArmLength;
	CameraBoom->bEnableCameraLag = true;
	CameraBoom->CameraLagSpeed = 8.f;
	CameraBoom->bUsePawnControlRotation = true;
		
	FollowCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("Camera"));
	FollowCamera->SetupAttachment(CameraBoom, USpringArmComponent::SocketName);
	FollowCamera->bUsePawnControlRotation = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationPitch = false;
	bUseControllerRotationRoll = false;

	GetMesh()->SetCollisionResponseToChannel(ECollisionChannel::ECC_Camera, ECollisionResponse::ECR_Ignore);
	GetMesh()->SetCollisionResponseToChannel(ECollisionChannel::ECC_Visibility, ECollisionResponse::ECR_Block);
	GetMesh()->SetCollisionObjectType(ECC_SkeletalMesh);
	GetCapsuleComponent()->SetCollisionResponseToChannel(ECollisionChannel::ECC_Camera, ECollisionResponse::ECR_Ignore);
	GetMesh()->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
	
	GetCharacterMovement()->bOrientRotationToMovement = true;
	GetCharacterMovement()->NavAgentProps.bCanCrouch = true;
	
	OverheadWidget = CreateDefaultSubobject<UWidgetComponent>(TEXT("OverheadWidget"));
	OverheadWidget->SetupAttachment(RootComponent);

	CombatComponent = CreateDefaultSubobject<UCombatComponent>(TEXT("CombatComponent"));
	//CombatComponent->InitCombatComp(this);
	CombatComponent->SetIsReplicated(true);

	BuffComponent = CreateDefaultSubobject<UBuffComponent>(TEXT("BuffComponent"));
	//BuffComponent->InitComponent(this);
	BuffComponent->SetIsReplicated(true);
	
	AttachedGrenade = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("AttachedGrenade"));
	AttachedGrenade->SetupAttachment(GetMesh(),FName("GrenadeSocket"));
	AttachedGrenade->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	AttachedGrenade->SetVisibility(false);
	
	TurnState = ETurnInPlace::ETIP_NoTurnning;
	NetUpdateFrequency = 66.f;
	MinNetUpdateFrequency = 30.f;
	Health = 100.f;

	TimelineComponent = CreateDefaultSubobject<UTimelineComponent>(TEXT("TimelineComp"));
}

// Called when the game starts or when spawned
void AShooterCharacter::BeginPlay()
{
	Super::BeginPlay();
	UpdateHealthHUD();
	UpdateShieldHUD();
	if (HasAuthority()) {
		OnTakeAnyDamage.AddDynamic(this, &ThisClass::RecevieDamageEvent);
	} 
	
	if (DissolveMaterialInstance) {
		DynamicDissolveMaterialInstance = UMaterialInstanceDynamic::Create(DissolveMaterialInstance, this);
		if (DynamicDissolveMaterialInstance) {
			GetMesh()->SetMaterial(0, DynamicDissolveMaterialInstance);
		}
	}

	IntoUnbeatableTime();
}

// Called every frame
void AShooterCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	CalcuAimOffset(DeltaTime);

	HideSelfIfCameraClosed();
}

// Called to bind functionality to input
void AShooterCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	if (PlayerInputComponent != nullptr) {
		PlayerInputComponent->BindAction(FName("Jump"),EInputEvent::IE_Pressed,this, &ThisClass::Jump);
		PlayerInputComponent->BindAxis(FName("MoveForward"), this, &ThisClass::MoveForward);
		PlayerInputComponent->BindAxis(FName("MoveRight"), this, &ThisClass::MoveRight);
		PlayerInputComponent->BindAxis(FName("LookUp"), this, &ThisClass::LookUp);
		PlayerInputComponent->BindAxis(FName("Turn"), this, &ThisClass::TurnAround);
		PlayerInputComponent->BindAction(FName("Equip"), EInputEvent::IE_Pressed, this, &ThisClass::EquipWeapon);
		PlayerInputComponent->BindAction(FName("Crouch"), EInputEvent::IE_Pressed, this, &ThisClass::CrouchPressed);
		PlayerInputComponent->BindAction(FName("Crouch"), EInputEvent::IE_Released, this, &ThisClass::CrouchPressed);
		PlayerInputComponent->BindAction(FName("ToggleWeapon"), EInputEvent::IE_Pressed, this, &ThisClass::ToggleWeapon);
		
		if (CombatComponent) {
			PlayerInputComponent->BindAction(FName("Aim"), EInputEvent::IE_Pressed, CombatComponent, &UCombatComponent::AimButtonPressed);
			PlayerInputComponent->BindAction(FName("Aim"), EInputEvent::IE_Released, CombatComponent, &UCombatComponent::AimButtonReleased);
			PlayerInputComponent->BindAction(FName("Fire"), EInputEvent::IE_Pressed, CombatComponent, &UCombatComponent::FireButtonPressed);
			PlayerInputComponent->BindAction(FName("Fire"), EInputEvent::IE_Released, CombatComponent, &UCombatComponent::FireButtonReleased);
			PlayerInputComponent->BindAction(FName("Reload"), EInputEvent::IE_Pressed, CombatComponent, &UCombatComponent::Reload);
			PlayerInputComponent->BindAction(FName("ThrowGrenade"), EInputEvent::IE_Pressed, CombatComponent, &UCombatComponent::ThrowGrenade);
			PlayerInputComponent->BindAction(FName("Sprint"), EInputEvent::IE_Pressed, CombatComponent, &UCombatComponent::SprintButtonPress);
			PlayerInputComponent->BindAction(FName("Sprint"), EInputEvent::IE_Released, CombatComponent, &UCombatComponent::SprintButtonRelease);
			PlayerInputComponent->BindAction(FName("Drop"), EInputEvent::IE_Pressed, this, &ThisClass::DropCurrentWeapon);
		}
	}
}

void AShooterCharacter::SetOverlapWeapon(AWeapon* InWeapon)
{
	// Sever Logic Set PickupText
	if (this->OverlapWeapon) {
		OverlapWeapon->SetPickupVisible(false);
	}
	
	this->OverlapWeapon = InWeapon;

	// just Locally Controller Set true; thinking point
	if (IsLocallyControlled()) {
		if (OverlapWeapon) {
			OverlapWeapon->SetPickupVisible(true);
		}
	}
}

void AShooterCharacter::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
	DOREPLIFETIME_CONDITION(ThisClass, OverlapWeapon, COND_OwnerOnly);
	DOREPLIFETIME(ThisClass, bEquipped);
	DOREPLIFETIME(ThisClass, Health);
	DOREPLIFETIME(ThisClass, bUnbeatable);
	DOREPLIFETIME(ThisClass, Shield);
}

void AShooterCharacter::SetPlayerDefaults()
{
	Super::SetPlayerDefaults();

	bEliminated = false;

	
	UpdateHealthHUD();
	UpdateShieldHUD();	
}

void AShooterCharacter::PossessedBy(AController* NewController)
{
	Super::PossessedBy(NewController);
	
	this->ShooterController = Cast<AShooterController>(NewController);

	if (CombatComponent) {
		CombatComponent->OnRep_GrenadeAmount();
	}

	IntoUnbeatableTime();
}

void AShooterCharacter::PostInitializeComponents()
{
	Super::PostInitializeComponents();

	if (CombatComponent) {
		CombatComponent->InitCombatComp(this);
	}

	if (BuffComponent) {
		BuffComponent->InitComponent(this);
	}
}


void AShooterCharacter::MoveForward(float Value)
{
	if (Controller) {
		// use Controller Rotator ensure the Forward direction
		FRotator Rotation = FRotator(0, GetControlRotation().Yaw, 0.f);
		FVector ForwardDir = FVector(FRotationMatrix(Rotation).GetUnitAxis(EAxis::X));
		AddMovementInput(ForwardDir, Value);
	}	
}

void AShooterCharacter::MoveRight(float Value)
{
	if (Controller) {
		FRotator Rotation = FRotator(0, GetControlRotation().Yaw, 0.f);
		FVector RightdDir = FVector(FRotationMatrix(Rotation).GetUnitAxis(EAxis::Y));
		AddMovementInput(RightdDir, Value);
	}	
}

void AShooterCharacter::TurnAround(float Value)
{
	AddControllerYawInput(Value);
}

void AShooterCharacter::LookUp(float Value)
{
	AddControllerPitchInput(Value);
}

void AShooterCharacter::CrouchPressed()
{
	if (bIsCrouched) {
		UnCrouch();
	} else {
		Crouch();
	}
}

// OnRep_function just call when var changed, and just call on client;
void AShooterCharacter::OnRep_OverlapWeapon(AWeapon* LastOverlapWeapon)
{
	if (LastOverlapWeapon) {
		LastOverlapWeapon->SetPickupVisible(false);
	}

	if (OverlapWeapon) {
		OverlapWeapon->SetPickupVisible(true);
	} 
}

// when we get equipment set fixed mode;
void AShooterCharacter::OnRep_Equipped()
{
	SetFixedView(bEquipped);

	if (bEquipped) {
		if (ShooterController) {
			ShooterController->SetAmmoHintVisible(true);
		}
	} else {
		if (ShooterController) {
			ShooterController->SetAmmoHintVisible(false);
		}
	}
}

bool AShooterCharacter::IsSprint() const
{
	if (CombatComponent) {
		return CombatComponent->GetCombatState() == ECombatState::ECS_Sprint;
	}
	return false;
}

void AShooterCharacter::SetIsEquipped(bool IsEquipped)
{
	bEquipped = IsEquipped;
	// Listen server need call this;
	OnRep_Equipped();
}

bool AShooterCharacter::IsAimming() const
{
	if (CombatComponent) {
		return CombatComponent->IsAiming();
	}
	return false;
}

void AShooterCharacter::SniperRifleZooming_Implementation(bool bAiming)
{
	if (bAiming) {
		CameraBoom->TargetArmLength = 0.f;
	} else {
		CameraBoom->TargetArmLength = this->TargetArmLength;
	}
}

void AShooterCharacter::SetFixedView(bool bFixed)
{
	if (bFixed) {
		GetCharacterMovement()->bOrientRotationToMovement = false;
		bUseControllerRotationYaw = true;
	} else {
		GetCharacterMovement()->bOrientRotationToMovement = true;
		bUseControllerRotationYaw = false;
	}
}

const AWeapon* AShooterCharacter::GetEquippedWeapon() const
{
	if (CombatComponent) {
		return CombatComponent->GetEquippedWeapon();
	}	
	return nullptr;
}

const AWeapon* AShooterCharacter::GetKnapsackWeapon() const
{
	if (CombatComponent) {
		return CombatComponent->GetKnapsackWeapon();
	}	
	return nullptr;
}


void AShooterCharacter::EquipWeapon()
{
	if (CombatComponent && OverlapWeapon) {
		CombatComponent->Server_EquipWeapon(OverlapWeapon);
	}	
}

void AShooterCharacter::DropCurrentWeapon()
{
	// just call this in the Autonomous;
	if (!IsLocallyControlled()) return;

	if (CombatComponent) {
		// if (GetEquippedWeapon() && GetEquippedWeapon()->GetWeaponType() == EWeaponTypes::EWT_SniperRife && IsAimming()) {
		// 	SniperRifleZooming(false);
		// }
		CombatComponent->Server_DropWeapon();
	}
}

void AShooterCharacter::DropAllWeapon()
{
	if (CombatComponent) {
		// if (GetEquippedWeapon() && GetEquippedWeapon()->GetWeaponType() == EWeaponTypes::EWT_SniperRife && IsAimming()) {
		// 	SniperRifleZooming(false);
		// }
		CombatComponent->DropCurrentWeapon();
		CombatComponent->DropKnapsackWeapon();
	}
}

void AShooterCharacter::ToggleWeapon()
{
	if (!IsLocallyControlled()) return;
	if (GetEquippedWeapon() && GetEquippedWeapon()->GetWeaponType() == EWeaponTypes::EWT_SniperRife && IsAimming()) {
		SniperRifleZooming(false);
	}
	if (CombatComponent) {
		CombatComponent->Server_TogglePackWeapon();	
	}
}

void AShooterCharacter::CalcuAimOffset(float DeltaTime)
{
	if (bEquipped == false) {
		bUseControllerRotationYaw = false;
		return;
	}
	FVector SpeedVec = GetVelocity();
	SpeedVec.Z = 0;
	float Speed = SpeedVec.Size();

	// Standing and not jumping
	if (Speed == 0.f && !GetCharacterMovement()->IsFalling()) {
		bUseControllerRotationYaw = true;
		
		FRotator CurRotation = GetBaseAimRotation();
		FRotator DeltaRotation = UKismetMathLibrary::NormalizedDeltaRotator(CurRotation, StartingAimRotation);
	
		AO_Yaw = DeltaRotation.Yaw;
		if (TurnState == ETurnInPlace::ETIP_NoTurnning) {
			InterpAO_Yaw = AO_Yaw;
		}
		TurnInPlace(DeltaTime);

		// if (IsLocallyControlled()) {
		// 	UE_LOG(LogTemp, Warning, TEXT("Server Yaw = %f, BaseYaw = %f, pitch=%f"), AO_Yaw, GetBaseAimRotation().Yaw, GetBaseAimRotation().Pitch);
		// 	
		// } else {
		// 	UE_LOG(LogTemp, Warning, TEXT("Client Yaw = %f, BaseYaw = %f, pitch=%f"), AO_Yaw, GetBaseAimRotation().Yaw, GetBaseAimRotation().Pitch);
		// }
	}
	else {
		StartingAimRotation = GetBaseAimRotation();
		AO_Yaw = 0.f;
		bUseControllerRotationYaw = true;
		TurnState = ETurnInPlace::ETIP_NoTurnning;
	}

	AO_Pitch = GetBaseAimRotation().Pitch;
	// GetBaseAimRotation() 在客户端和Server端不一致, 需要处理; 
	if (AO_Pitch > 90.f  && !IsLocallyControlled()) {
		// map pitch from [270, 360) to [-90, 0);
		FVector2D RangeSource(270, 360);
		FVector2D RangeTarget(-90, 0);
		AO_Pitch = FMath::GetMappedRangeValueClamped(RangeSource, RangeTarget, AO_Pitch);
	}
}

void AShooterCharacter::Jump()
{
	if (bIsCrouched) {
		Server_CrouchJump();
	} else {
		Super::Jump();
	}
}

void AShooterCharacter::HideSelfIfCameraClosed()
{
	if (!IsLocallyControlled() || CombatComponent == nullptr) return;

	if (FollowCamera && GetMesh()) {
		float CameraToCharacter = (FollowCamera->GetComponentLocation() - GetActorLocation()).Size();
		if (CameraToCharacter < CameraHideThreshold) {
			GetMesh()->SetVisibility(false);
			if (GetEquippedWeapon()) {
				GetEquippedWeapon()->SetVisible(false);
			}
			if (GetKnapsackWeapon()) {
				GetKnapsackWeapon()->SetVisible(false);
			}
		} else {
			GetMesh()->SetVisibility(true);
			if (GetEquippedWeapon()) {
				GetEquippedWeapon()->SetVisible(true);
			}
			if (GetKnapsackWeapon()) {
				GetKnapsackWeapon()->SetVisible(true);
			}
		}
	}
}

void AShooterCharacter::Server_CrouchJump_Implementation()
{
	float JumpZVel = GetCharacterMovement()->JumpZVelocity / 2;
	GetCharacterMovement()->AddImpulse(FVector(0, 0, JumpZVel), true);
}

void AShooterCharacter::TurnInPlace(float DeltaTime)
{
	if (AO_Yaw <= -80.f) {
		TurnState = ETurnInPlace::ETIP_TurnLeft;
	}
	else if (AO_Yaw >= 80.f) {
		TurnState = ETurnInPlace::ETIP_TurnRight;
	}
	// Begin Turning Interp Yaw;
	if (TurnState != ETurnInPlace::ETIP_NoTurnning) {
		InterpAO_Yaw = FMath::FInterpTo(InterpAO_Yaw, 0, DeltaTime, 5.f);
		AO_Yaw = InterpAO_Yaw;

		if (abs(AO_Yaw) <= 15.f) {
			TurnState = ETurnInPlace::ETIP_NoTurnning;
			StartingAimRotation = GetBaseAimRotation();
		}
	}
}

float AShooterCharacter::GetCameraFOV() const
{
	if (FollowCamera) {
		return FollowCamera->FieldOfView;
	}
	return 30.f;
}

void AShooterCharacter::SetCameraFOV(float FieldOfView)
{
	if (FollowCamera) {
		FollowCamera->SetFieldOfView(FieldOfView);
	}
}

FVector AShooterCharacter::GetCameraLocation() const
{
	if (FollowCamera) {
		return FollowCamera->GetComponentLocation();
	}
	return FVector();
}

void AShooterCharacter::RecevieDamageEvent(AActor* DamagedActor, float Damage, const UDamageType* DamageType, AController* InstigatedBy, AActor* DamageCauser)
{
	// if We has been Eliminated, not send msg to server;
	if (bEliminated || !HasAuthority() || IsUnbeatable()) return;

	const float LastHealth = Health;

	// 当前Shield 越接近MaxShield, 减伤越多;
	float ActualDamage = Damage - Damage * (Shield * 1.0f / MaxShield) * 0.5f;
	Shield = FMath::Clamp<int32>(Shield - FMath::CeilToInt(Damage), 0, MaxShield);
	OnRep_Shield();

	Health = FMath::Clamp(Health - ActualDamage, 0.f, MaxHealth);
	UWorld *World = GetWorld();

	if (World && Health <= 0.f) {
		AShooterGameMode* ShooterGameMode = World->GetAuthGameMode<AShooterGameMode>();
		if (ShooterGameMode) {
			AShooterController* AttackerController = Cast<AShooterController>(InstigatedBy);
			ShooterGameMode->EliminatePlayer(this, ShooterController, AttackerController);
		}
	}
	
	// Because take Damage Happen in the server so we need call this again;
	OnRep_Health(LastHealth);
}

void AShooterCharacter::Eliminated()
{
	bEliminated = true;
	
	Multi_Eliminated();
	
	SetOverlapWeapon(nullptr);

	GetWorldTimerManager().SetTimer(RespawnTimerHandle, this, &ThisClass::OnEliminateTimerFinish, RespawnRate);
	
}

void AShooterCharacter::Multi_Eliminated_Implementation()
{
	DropAllWeapon();
	
	// Disable Character Input and move
	GetCharacterMovement()->DisableMovement();
	GetCharacterMovement()->StopMovementImmediately();
	if (ShooterController) {
		DisableInput(ShooterController);
	}
	GetCapsuleComponent()->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	GetMesh()->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	
	bEliminated = true;
	PlayEliminatedMontage();
	StartTimeline();

	if (ElimParticle) {
		FVector SpawnLocation = GetActorLocation();
		SpawnLocation.Z += 200.f;
		ElimParticleSystemComponent = UGameplayStatics::SpawnEmitterAtLocation(
			this,
			ElimParticle,
			SpawnLocation,
			GetActorRotation()
			);
	}

	if (ElimBotSound) {
		UGameplayStatics::SpawnSoundAtLocation(
			this,
			ElimBotSound,
			GetActorLocation()
			);
	}

}

void AShooterCharacter::OnRep_Health(float LastHealth)
{
	UpdateHealthHUD();
	if (Health < LastHealth) {
		PlayHitReact();
	}
}

void AShooterCharacter::UpdateHealthHUD() {

	ShooterController = Cast<AShooterController>(GetController());

	if (ShooterController) {
		ShooterController->SetHUDHealth(Health);
	}
}

void AShooterCharacter::OnRep_Shield()
{
	UpdateShieldHUD();
}

void AShooterCharacter::UpdateShieldHUD()
{
	ShooterController = Cast<AShooterController>(GetController());

	if (ShooterController) {
		ShooterController->SetHUDShield(Shield);
	}
		
}

void AShooterCharacter::SetCurHealth(float NewHealth) 
{
	if (!HasAuthority()) return;
	const float LastHealth = Health;
	Health = FMath::Clamp(NewHealth, 0.f, MaxHealth);
	OnRep_Health(LastHealth);	
}

void AShooterCharacter::SetCurShield(int32 NewShield)
{
	if (!HasAuthority()) return;
	Shield = FMath::Clamp<int32>(NewShield, 1, MaxShield);
	OnRep_Shield();
}

void AShooterCharacter::PlayReloadMontage()
{
	if (ReloadMontage && CombatComponent) {
		switch (CombatComponent->GetCurrentWeaponTypes()) {
			case EWeaponTypes::EWT_AssaultRife:
			{
				PlayAnimMontage(ReloadMontage,1.f, FName("AssaultRife"));
			}break;
			case EWeaponTypes::EWT_RocketLauncher:
			{
				PlayAnimMontage(ReloadMontage, 1.f, FName("RocketLauncher"));
			}break;
			case EWeaponTypes::EWT_Pistol:
			{
				PlayAnimMontage(ReloadMontage, 1.f, FName("Pistol"));
			}break;
			case EWeaponTypes::EWT_Shotgun:
			{
				PlayAnimMontage(ReloadMontage, 1.f, FName("Shotgun"));
			}break;
			case EWeaponTypes::EWT_SniperRife:
			{
				PlayAnimMontage(ReloadMontage, 1.f, FName("SniperRife"));
			}break;
			case EWeaponTypes::EWT_GrenadeLauncher:
			{
				PlayAnimMontage(ReloadMontage, 1.f, FName("GrenadeLauncher"));
			}break;
			default:
			{
				UE_LOG(LogTemp, Warning, TEXT("CurrentWeaponType Unknown!"));
			}break;
		}
	}	
}

void AShooterCharacter::PlayThrowGrenadeMontage()
{
	if (ThrowGrenadeMontage) {
		PlayAnimMontage(ThrowGrenadeMontage);
	}
}

void AShooterCharacter::OnReloadFinished()
{
	if (CombatComponent) {
		CombatComponent->FinishReloading();
	}
}

void AShooterCharacter::OnReloadShotgun()
{
	if (GetEquippedWeapon() == nullptr || GetEquippedWeapon()->ClipFull()) return ;
	if (CombatComponent) {
		CombatComponent->ShotgunAddAmmo();
	}	
}

void AShooterCharacter::OnThrowGrenadeEnd()
{
	if (CombatComponent) {
		CombatComponent->FinishThrowGrenade();	
	}
}

void AShooterCharacter::SetAttachedGrenadeVisible(bool bVisible)
{
	if (AttachedGrenade) {
		AttachedGrenade->SetVisibility(bVisible);
	}
}

FVector AShooterCharacter::GetAttachedGrenadeLoc() const
{
	if (AttachedGrenade) {
		return AttachedGrenade->GetComponentLocation();
	}
	return FVector();
}

void AShooterCharacter::PickUpAmmo(EWeaponTypes WeaponType, int32 AddAmmo)
{
	if (CombatComponent) {
		CombatComponent->PickUpAmmo(WeaponType, AddAmmo);
	}
}

void AShooterCharacter::IntoUnbeatableTime()
{
	bUnbeatable = true;
	GetMesh()->bRenderCustomDepth = true;
	GetMesh()->SetCustomDepthStencilValue(CUSTOM_DEPTH_TAN);
	GetMesh()->MarkRenderStateDirty();
	if (HasAuthority()) {
		GetWorldTimerManager().SetTimer(RespawnTimerHandle, this, &ThisClass::OnUnbeatableTimeEnd, UnbeatableTime);
	}
}

void AShooterCharacter::OnUnbeatableTimeEnd()
{
	bUnbeatable = false;
	GetMesh()->bRenderCustomDepth = false;
	GetMesh()->MarkRenderStateDirty();
}

void AShooterCharacter::OnRep_Unbeatable()
{
	if (bUnbeatable) {
		IntoUnbeatableTime();	
	} else {
		GetMesh()->bRenderCustomDepth = false;
		GetMesh()->MarkRenderStateDirty();
	}
}

void AShooterCharacter::JumpToReloadEnd_Implementation()
{
	if (CombatComponent == nullptr || CombatComponent->GetEquippedWeapon() == nullptr) return;
	if (CombatComponent->CarriedAmmo == 0 || CombatComponent->GetEquippedWeapon()->ClipFull()) {
		if (ReloadMontage && GetMesh()) {
			UAnimInstance* AnimInstance = GetMesh()->GetAnimInstance();
			if (AnimInstance) {
				AnimInstance->Montage_JumpToSection(FName("ShotgunEnd"));
			}
		}
	}
}

void AShooterCharacter::PlayHitReact()
{
	if (HitReactMontage) {
		FName StartSection("HitFwd");
		PlayAnimMontage(HitReactMontage, 1, StartSection);
	}
	if (HitReactParticle) {
		UGameplayStatics::SpawnEmitterAtLocation(this, HitReactParticle, GetActorLocation());
	}
	
}

void AShooterCharacter::PlayEliminatedMontage()
{
	if (EliminatedMontage) {
		UShooterAnimInstance* AnimInstance = Cast<UShooterAnimInstance>(GetMesh()->GetAnimInstance());
		if (AnimInstance) {
			AnimInstance->Montage_Play(EliminatedMontage);
		}
	}
}

void AShooterCharacter::OnEliminateTimerFinish()
{
	AShooterGameMode* ShooterGameMode = GetWorld()->GetAuthGameMode<AShooterGameMode>();
	if (ShooterGameMode) {
		ShooterGameMode->RequestRespawn(this, ShooterController);
	}
}

void AShooterCharacter::UpdateDissolveCurve(float CurrentValue)
{
	// UE_LOG(LogTemp, Warning, TEXT("CurrentValue=%f"), CurrentValue);

	if (DynamicDissolveMaterialInstance) {
		DynamicDissolveMaterialInstance->SetScalarParameterValue(FName("Dissolve"), CurrentValue);
	}
}

void AShooterCharacter::StartTimeline()
{
	OnTimelineFloat.BindDynamic(this, &ThisClass::UpdateDissolveCurve);

	if (TimelineComponent && DissolveCurveFloat) {
		TimelineComponent->AddInterpFloat(DissolveCurveFloat, OnTimelineFloat);
		TimelineComponent->PlayFromStart();
	}
}
