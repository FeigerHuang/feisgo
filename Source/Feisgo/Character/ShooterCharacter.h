// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "ShooterController.h"
#include "Camera/CameraComponent.h"
#include "Components/TimelineComponent.h"
#include "Components/WidgetComponent.h"
#include "Feisgo/GamePlay/Weapon.h"
#include "Feisgo/Interface/CrosshairReactInterface.h"
#include "Feisgo/ShooterComponent/BuffComponent.h"
#include "GameFramework/Character.h"
#include "GameFramework/SpringArmComponent.h"
#include "Feisgo/ShooterTypes/TurnType.h"
#include "ShooterCharacter.generated.h"

UCLASS()
class FEISGO_API AShooterCharacter : public ACharacter, public ICrosshairReactInterface
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	AShooterCharacter();
	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	// just be call on the server;
	UFUNCTION(BlueprintAuthorityOnly)
	void SetOverlapWeapon(AWeapon* InWeapon);

	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;

	virtual void SetPlayerDefaults() override;

	virtual void PossessedBy(AController* NewController) override;

	/** Allow actors to initialize themselves on the C++ side after all of their components have been initialized, only called during gameplay */
	virtual void PostInitializeComponents() override;

	UFUNCTION(BlueprintImplementableEvent)
	void ShowDisplayName();
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	UPROPERTY(VisibleAnywhere,BlueprintReadOnly, Category = "Camera")
	USpringArmComponent* CameraBoom;

	UPROPERTY(VisibleAnywhere,BlueprintReadOnly, Category = "Camera")
	UCameraComponent* FollowCamera;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	UCombatComponent* CombatComponent;

	UPROPERTY(VisibleAnywhere)
	UBuffComponent* BuffComponent;

	void MoveForward(float Value);

	void MoveRight(float Value);

	void TurnAround(float Value);

	void LookUp(float Value);

	void CrouchPressed();
	
	void EquipWeapon();

	void DropCurrentWeapon();

	// must call in all machine
	void DropAllWeapon();
	
	void ToggleWeapon();
	
	void CalcuAimOffset(float DeltaTime);

	virtual void Jump() override;

	UFUNCTION(Server, Reliable)
	void Server_CrouchJump();

	void HideSelfIfCameraClosed();
	
	UPROPERTY(EditAnywhere)
	float CameraHideThreshold = 200.f;

private:

	UPROPERTY(EditAnywhere, BlueprintReadOnly, meta=(AllowPrivateAccess))
	UWidgetComponent* OverheadWidget;

	UPROPERTY(ReplicatedUsing=OnRep_OverlapWeapon)
	AWeapon* OverlapWeapon;

	UPROPERTY(ReplicatedUsing=OnRep_Equipped)
	bool bEquipped = false;
	
	UFUNCTION()
	void OnRep_OverlapWeapon(AWeapon* LastOverlapWeapon);

	UFUNCTION()
	void OnRep_Equipped();

	float AO_Yaw;
	float InterpAO_Yaw;
	float AO_Pitch;
	
	UPROPERTY(EditAnywhere)
	float TargetArmLength = 500.f;
	
	FRotator StartingAimRotation;

	ETurnInPlace TurnState;

public:
	
 	FORCEINLINE bool IsEquipped() const {return bEquipped;}

	bool IsSprint() const;
	
	void SetIsEquipped(bool IsEquipped);

	bool IsAimming() const;

	UFUNCTION(BlueprintNativeEvent)
	void SniperRifleZooming(bool bAiming);

	UFUNCTION(BlueprintCallable)
	void SetFixedView(bool bFixed);

	FORCEINLINE float GetAimOffsetsYaw() const {return AO_Yaw;}

	FORCEINLINE float GetAimOffsetsPitch() const {return AO_Pitch;}

	const AWeapon* GetEquippedWeapon() const;
	const AWeapon* GetKnapsackWeapon() const;
	
	FORCEINLINE ETurnInPlace GetTurnState() const {return TurnState;}

	void TurnInPlace(float Yaw);
	// When Aim, when need zoomed our Camera FOV
	float GetCameraFOV() const;

	void SetCameraFOV(float FieldOfView);

	FVector GetCameraLocation() const;
	
	void UpdateHealthHUD();
	
	UFUNCTION()
	void OnRep_Health(float LastHealth);

	void UpdateShieldHUD();
	
	UFUNCTION()
	void OnRep_Shield();
	
	UFUNCTION()
	void RecevieDamageEvent(
		AActor* DamagedActor,
		float Damage,
		const class UDamageType* DamageType,
		class AController* InstigatedBy,
		AActor* DamageCauser);


	// Eliminated just Call in the server;
	void Eliminated();
	
	// Eliminate Event
	UFUNCTION(NetMulticast, Reliable)
	void Multi_Eliminated();

	FORCEINLINE bool GetEliminated() const {return bEliminated;}
	FORCEINLINE UBuffComponent* GetBuffComponent() const {return BuffComponent;}
	FORCEINLINE float GetCurHealth() const {return Health;}
	FORCEINLINE float GetMaxHealth() const {return MaxHealth;}
	FORCEINLINE int32 GetCurShield() const {return  Shield;}
	FORCEINLINE int32 GetMaxShield() const {return MaxShield;}
	// Set Health And Shield just happen in the server;
	void SetCurHealth(float NewHealth); 

	void SetCurShield(int32 NewShield);
	
	void PlayReloadMontage();

	void PlayThrowGrenadeMontage();
	
	UFUNCTION(BlueprintAuthorityOnly, BlueprintCallable)
	void OnReloadFinished();

	UFUNCTION(BlueprintAuthorityOnly, BlueprintCallable)
	void OnReloadShotgun();

	UFUNCTION(NetMulticast, Unreliable)
	void JumpToReloadEnd();
	
	UFUNCTION(BlueprintAuthorityOnly, BlueprintCallable)
	void OnThrowGrenadeEnd();

	UFUNCTION(BlueprintCallable)
	void SetAttachedGrenadeVisible(bool bVisible);

	FVector GetAttachedGrenadeLoc() const;
	
	UPROPERTY(EditAnywhere, Category=Grenade)
	UStaticMeshComponent* AttachedGrenade;

	void PickUpAmmo(EWeaponTypes WeaponType, int32 AddAmmo);

	void IntoUnbeatableTime();

	void OnUnbeatableTimeEnd();

	UFUNCTION()
	void OnRep_Unbeatable();

	FORCEINLINE bool IsUnbeatable() const {return bUnbeatable;}
private:
	FTimerHandle RespawnTimerHandle;

	UPROPERTY(EditAnywhere)
	float UnbeatableTime = 5.f;

	UPROPERTY(ReplicatedUsing=OnRep_Unbeatable)
	bool bUnbeatable = false;
	/***
	 * Montage Relative;
	 */
	UPROPERTY(EditAnywhere)
	UAnimMontage* ReloadMontage;
	
	UPROPERTY(EditAnywhere)
	UAnimMontage* HitReactMontage;
	
	UPROPERTY(EditAnywhere)
	UAnimMontage* EliminatedMontage;

	UPROPERTY(EditAnywhere)
	UAnimMontage* ThrowGrenadeMontage;

	UPROPERTY(EditAnywhere)
	UParticleSystem* HitReactParticle;
	
	void PlayHitReact();

	void PlayEliminatedMontage();

	bool bEliminated = false;

	UPROPERTY(EditDefaultsOnly)
	float RespawnRate = 3.0f;

	/**
	 * Health
	 */
	UPROPERTY(EditAnywhere)
	float MaxHealth = 100.f;
	
	UPROPERTY(EditAnywhere, ReplicatedUsing=OnRep_Health)
	float Health;

	/**
	 * Shield
	 */
	UPROPERTY(EditAnywhere, meta=(ClampMin=1,ClampMax=100))
	int32 MaxShield = 100;
	
	UPROPERTY(EditAnywhere, ReplicatedUsing=OnRep_Shield)
	int32 Shield;

	AShooterController* ShooterController;
	
	void OnEliminateTimerFinish();

	/**
	 * Character Dissolve
	 */
	UTimelineComponent* TimelineComponent;

	UPROPERTY(EditAnywhere, Category=Elim)
	UCurveFloat* DissolveCurveFloat;

	FOnTimelineFloat OnTimelineFloat;

	UPROPERTY(EditDefaultsOnly, Category=Elim)
	UMaterialInstance* DissolveMaterialInstance;

	UMaterialInstanceDynamic* DynamicDissolveMaterialInstance;
	
	UFUNCTION()
	void UpdateDissolveCurve(float CurrentValue);

	void StartTimeline();

	/**
	 * Elim Bot
	 */
	UPROPERTY(EditAnywhere)
	UParticleSystem*  ElimParticle;

	UPROPERTY(VisibleAnywhere)
	UParticleSystemComponent* ElimParticleSystemComponent;

	UPROPERTY(EditAnywhere)
	USoundCue* ElimBotSound;
};




