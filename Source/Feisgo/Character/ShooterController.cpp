// Fill out your copyright notice in the Description page of Project Settings.


#include "ShooterController.h"

#include "ShooterState.h"
#include "Feisgo/Framework/ShooterGameInstance.h"
#include "Feisgo/Framework/ShooterGameMode.h"
#include "Feisgo/Framework/ShooterGameState.h"
#include "Feisgo/UI/ShooterHUD.h"
#include "GameFramework/GameMode.h"
#include "Kismet/GameplayStatics.h"
#include "Net/UnrealNetwork.h"


void AShooterController::BeginPlay()
{
	Super::BeginPlay();

	ShooterHUD = Cast<AShooterHUD>(GetHUD());

	Server_CheckMatchState();
}

void AShooterController::Server_CheckMatchState_Implementation()
{
	AShooterGameMode* GameMode = Cast<AShooterGameMode>(UGameplayStatics::GetGameMode(this));
	if (GameMode) {
		WarmupTime = GameMode->GetWarmupTime();
		MatchTime = GameMode->GetMatchTime();
		LevelStartTime = GameMode->GetLevelStartTime();
		CurMatchState = GameMode->GetMatchState();
		CooldownTime = GameMode->GetCooldownTime();
		Client_JoinMidGame(CurMatchState, WarmupTime, MatchTime, LevelStartTime, CooldownTime);
	}
}

void AShooterController::Client_JoinMidGame_Implementation(FName State, float InWarmupTime, float InMatchTime, float InLevelStartTime, float InCooldown)
{
	CurMatchState = State;
	WarmupTime = InWarmupTime;
	MatchTime = InMatchTime;
	LevelStartTime = InLevelStartTime;
	CooldownTime = InCooldown;
}

void AShooterController::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);

	SetHUDTime();

	CheckTimeSync(DeltaSeconds);
}

void AShooterController::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
	DOREPLIFETIME(ThisClass, CurMatchState);
}

void AShooterController::OnMatchStateSet(FName State)
{
	this->CurMatchState = State;
	OnRep_CurMatchState();
}

void AShooterController::OnRep_CurMatchState()
{
	if (CurMatchState == MatchState::InProgress) {
		if (ShooterHUD) {
			ShooterHUD->SetAnnouncementVisible(false);
			ShooterHUD->SetShooterOverlayVisible(true);
		}
	} else if (CurMatchState == MatchState::Cooldown) {
		if (ShooterHUD) {
			ShooterHUD->SetAnnouncementVisible(true);
			FString AnnoString(TEXT("New Match Starts in"));
			ShooterHUD->SetAnnouncementText(FText::FromString(AnnoString));
			ShooterHUD->SetShooterOverlayVisible(false);
			
			ShowRankList();		
		}
	}
}

void AShooterController::ShowRankList()
{
		AShooterGameState* ShooterGameState = Cast<AShooterGameState>(UGameplayStatics::GetGameState(this));
		if (ShooterGameState) {
			TArray<AShooterState*> RankResult;
			FString                ListInfo("Kill Rank List : \n");
			ShooterGameState->GetRankInfo(RankResult);

			int32 LastPos = FMath::Min(3, RankResult.Num());
			FString Suffix[3] = {TEXT("st"), TEXT("nd"), TEXT("rd")};
			for (int32 i = 0; i < LastPos; ++i) {
				ListInfo += FString::Printf(TEXT("%d%s Shooter : %s |KillAmount=%d\n"), i + 1, *Suffix[i], *(RankResult[i]->GetPlayerName()), RankResult[i]->GetKillAmount());
			}
		ShooterHUD->SetInfoText(FText::FromString(ListInfo));
	}
}


void AShooterController::ReceivedPlayer()
{
	Super::ReceivedPlayer();

	if (IsLocalController()) {
		Server_RequestServerTime(GetWorld()->GetTimeSeconds());

		UShooterGameInstance* ShooterGameInstance = GetGameInstance<UShooterGameInstance>();
		if (ShooterGameInstance) {
			ShooterGameInstance->UpdatePlayerName();
		}
	} 
}


void AShooterController::Server_RequestServerTime_Implementation(float TimeOfClientRequest)
{
	Client_ReportServerTime(TimeOfClientRequest, GetWorld()->GetTimeSeconds());	
}

void AShooterController::Client_ReportServerTime_Implementation(float TimeOfClientRequest, float ServerTime)
{
	float RoundTripTime = GetWorld()->GetTimeSeconds() - TimeOfClientRequest;
	float CurrentServerTime = ServerTime + 0.5 * RoundTripTime;
	ClientServerDelta = CurrentServerTime - GetWorld()->GetTimeSeconds();
}

float AShooterController::GetServerTime()
{
	if (HasAuthority()) return GetWorld()->GetTimeSeconds();
	return GetWorld()->GetTimeSeconds() + ClientServerDelta;
}

void AShooterController::SetHUDTime()
{
	if (ShooterHUD == nullptr) return;
	
	float TimeLeft = 0.f;
	if (CurMatchState == MatchState::WaitingToStart) {
		TimeLeft = WarmupTime - (GetServerTime() - LevelStartTime);
	} else if (CurMatchState == MatchState::InProgress) {
		TimeLeft = MatchTime + WarmupTime - (GetServerTime() - LevelStartTime);
	} else if (CurMatchState == MatchState::Cooldown) {
		TimeLeft = MatchTime +WarmupTime + CooldownTime - (GetServerTime() - LevelStartTime);
	}
	
	uint32 SecondsLeft = FMath::CeilToInt(TimeLeft);

	if (CountdownInt != SecondsLeft) {
		if (CurMatchState == MatchState::WaitingToStart || CurMatchState == MatchState::Cooldown) {
			ShooterHUD->UpdateWarmupTime(SecondsLeft);	
		} else if (CurMatchState == MatchState::InProgress) {
			ShooterHUD->UpdateRemainTime(SecondsLeft);
		}
	}

	CountdownInt = SecondsLeft;
}


void AShooterController::CheckTimeSync(float DeltaTime)
{
	TimeSyncRunningTime += DeltaTime;
	if (IsLocalController() && TimeSyncFrequency >= TimeSyncFrequency) {
		Server_RequestServerTime(GetWorld()->GetTimeSeconds());
		TimeSyncRunningTime = 0.f;
	}
}

void AShooterController::SetHUDHealth(float NewHealth)
{
	ShooterHUD = (ShooterHUD == nullptr ? Cast<AShooterHUD>(GetHUD()) : ShooterHUD);

	if (ShooterHUD) {
		ShooterHUD->SetHealthText(NewHealth);
	}
}

void AShooterController::SetHUDShield(int32 Shield)
{
	ShooterHUD = (ShooterHUD == nullptr ? Cast<AShooterHUD>(GetHUD()) : ShooterHUD);

	if (ShooterHUD) {
		ShooterHUD->SetShieldText(Shield);
	}
}

void AShooterController::SetHUDKillAmount(int32 KillAmount)
{
	ShooterHUD = (ShooterHUD == nullptr ? Cast<AShooterHUD>(GetHUD()) : ShooterHUD);
	if (ShooterHUD) {
		ShooterHUD->UpdateKillAmount(KillAmount, KillAmountFadeTime);
	}
	
}

void AShooterController::SetHUDClipAmmo(int32 ClipAmmo)
{
	ShooterHUD = (ShooterHUD == nullptr ? Cast<AShooterHUD>(GetHUD()) : ShooterHUD);
	if (ShooterHUD) {
		ShooterHUD->SetAmmoHintVisible(true);
		ShooterHUD->UpdateClipAmmoAmount(ClipAmmo);
	}
	
}

void AShooterController::SetHUDCarriedAmmo(int32 CarriedAmmo)
{
	ShooterHUD = (ShooterHUD == nullptr ? Cast<AShooterHUD>(GetHUD()) : ShooterHUD);
	if (ShooterHUD) {
		ShooterHUD->SetAmmoHintVisible(true);
		ShooterHUD->UpdateCarriedAmmoAmount(CarriedAmmo);
	}
}

void AShooterController::SetHUDWeaponUI(EWeaponTypes NewWeaponType)
{
	ShooterHUD = (ShooterHUD == nullptr ? Cast<AShooterHUD>(GetHUD()) : ShooterHUD);
	if (ShooterHUD) {
		ShooterHUD->UpdateWeaponType(NewWeaponType);
	}
}

void AShooterController::SetAmmoHintVisible(bool bVisible)
{
	ShooterHUD = (ShooterHUD == nullptr ? Cast<AShooterHUD>(GetHUD()) : ShooterHUD);
	if (ShooterHUD) {
		ShooterHUD->SetAmmoHintVisible(bVisible);
	}
}

void AShooterController::SetHUDGrenadeAmount(int32 Amount)
{
	ShooterHUD = (ShooterHUD == nullptr ? Cast<AShooterHUD>(GetHUD()) : ShooterHUD);
	if (ShooterHUD) {
		ShooterHUD->SetGrenadeAmount(Amount);
	}
}


