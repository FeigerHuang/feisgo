// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Feisgo/ShooterTypes/WeaponTypes.h"
#include "GameFramework/PlayerController.h"
#include "ShooterController.generated.h"

class AShooterHUD;
/**
 * 
 */
UCLASS()
class FEISGO_API AShooterController : public APlayerController
{
	GENERATED_BODY()
public:
	virtual void Tick(float DeltaSeconds) override;

	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;

	// this called from GameMode
	void OnMatchStateSet(FName State);
	
	void SetHUDHealth(float NewHealth);

	void SetHUDShield(int32 Shield);
	
	void SetHUDKillAmount(int32 KillAmount);

	void SetHUDClipAmmo(int32 ClipAmmo);
	
	void SetHUDCarriedAmmo(int32 CarriedAmmo);

	void SetHUDWeaponUI(EWeaponTypes NewWeaponType);
	
	void SetAmmoHintVisible(bool bVisible);

	void SetHUDGrenadeAmount(int32 Amount);
	
	// Synced with Server World clock;
	virtual float GetServerTime();

	// Sync with Server clock as soon as Possible
	virtual void ReceivedPlayer() override;

protected:
	virtual void BeginPlay() override;

	UPROPERTY(EditAnywhere)
	float KillAmountFadeTime = 3.f;

	/**
	 * Sync time between client and server
	 */
	
	// Request the current server time, passing the client current time;
	UFUNCTION(Server, Reliable)
	void Server_RequestServerTime(float TimeOfClientRequest);

	//Reports the current server time to the client to response to RequestServerTime;
	UFUNCTION(Client, Reliable)
	void Client_ReportServerTime(float TimeOfClientRequest, float ServerTime);

	// difference between client and server time;
	float ClientServerDelta = 0.f;

	void CheckTimeSync(float DeltaTime);

	UPROPERTY(EditAnywhere, Category=Time)
	float TimeSyncFrequency = 5.f;

	float TimeSyncRunningTime = 0.f;
	
	void SetHUDTime();

	UFUNCTION()
	void OnRep_CurMatchState();

	UFUNCTION(Server, Reliable)
	void Server_CheckMatchState();

	UFUNCTION(Client, Reliable)
	void Client_JoinMidGame(FName State, float InWarmupTime, float InMatchTime, float InLevelStartTime, float InCooldown);

	void ShowRankList();

private:
	AShooterHUD* ShooterHUD;
	
	float MatchTime = 0.f;

	float WarmupTime = 0.f;

	float LevelStartTime = 0.f;

	float CooldownTime = 0.f;
	
	uint32 CountdownInt = 0;

	UPROPERTY(ReplicatedUsing=OnRep_CurMatchState)
	FName CurMatchState;
};

