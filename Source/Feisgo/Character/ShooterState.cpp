// Fill out your copyright notice in the Description page of Project Settings.


#include "ShooterState.h"

#include "ShooterCharacter.h"
#include "Net/UnrealNetwork.h"

void AShooterState::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
	DOREPLIFETIME(ThisClass, KillAmount);
}

void AShooterState::AddOneKill_NotifyServer()
{
	++KillAmount;
	OnRep_KillAmount();
}

void AShooterState::OnRep_KillAmount()
{
	if (ShooterController == nullptr) {
		ShooterController = Cast<AShooterController>(GetPawn<AShooterCharacter>()->Controller);
	}

	if (ShooterController) {
		ShooterController->SetHUDKillAmount(KillAmount);
	}
}
