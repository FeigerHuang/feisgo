// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "ShooterController.h"
#include "GameFramework/PlayerState.h"
#include "ShooterState.generated.h"

/**
 * 
 */
UCLASS()
class FEISGO_API AShooterState : public APlayerState
{
	GENERATED_BODY()
public:

	virtual void GetLifetimeReplicatedProps(TArray< FLifetimeProperty > & OutLifetimeProps) const override;
	
	void AddOneKill_NotifyServer();

	FORCEINLINE int32 GetKillAmount() const {return KillAmount;}
private:
	UPROPERTY(ReplicatedUsing=OnRep_KillAmount)
	int32 KillAmount;

	UFUNCTION()
	void OnRep_KillAmount();
	
	AShooterController* ShooterController;
};
