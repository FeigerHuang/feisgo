// Fill out your copyright notice in the Description page of Project Settings.


#include "LobbyGameMode.h"

#include "ShooterGameInstance.h"
#include "Feisgo/Character/ShooterCharacter.h"
#include "GameFramework/GameStateBase.h"


void ALobbyGameMode::BeginPlay()
{
	Super::BeginPlay();
	
	InitConnectionNumber();		

}

void ALobbyGameMode::SeamlessServerTravel() {
	UWorld* World = GetWorld();
	if (World) {
		bUseSeamlessTravel = true;
		World->ServerTravel("/Game/Maps/DeathMatchMap?listen");
	}
	OnSeamlessTravelEnd();
}

void ALobbyGameMode::StartToLeaveMap()
{
	Super::StartToLeaveMap();
		
}


// GameMode just exit in the server;
void ALobbyGameMode::PostLogin(APlayerController* NewPlayer)
{
	Super::PostLogin(NewPlayer);


	InitConnectionNumber();
	if (ShooterGameState != nullptr) {
		
		if (ShooterGameState->CurConnections == MaxPlayerNumber) {
			SeamlessServerTravel();
		}
	}

	if (GEngine) {
		GEngine->AddOnScreenDebugMessage(
			-1,
			10,
			FColor::Cyan,
			FString::Printf(TEXT("MaxPlayerNum=%d, curPlayer=%d"), MaxPlayerNumber, GameState->PlayerArray.Num())
		);
	}
}

void ALobbyGameMode::Logout(AController* Exiting)
{
	Super::Logout(Exiting);

	if (GameState != nullptr) {
		int32 NumOfPlayers = GameState->PlayerArray.Num() - 1;

		if (ShooterGameState) {
			ShooterGameState->CurConnections = NumOfPlayers;
		}
	}
}

void ALobbyGameMode::InitConnectionNumber() {
	UShooterGameInstance* ShooterGameInstance = GetGameInstance<UShooterGameInstance>();
	if (ShooterGameInstance) {
		ShooterGameState = GetGameState<AShooterGameState>();
		this->MaxPlayerNumber = ShooterGameInstance->GetMaxPlayerNumber();
		if (ShooterGameState) {
			ShooterGameState->MaxConnections = MaxPlayerNumber;
		}
	}

	if (GameState != nullptr) {
		int32 NumOfPlayers = GameState->PlayerArray.Num();

		if (ShooterGameState) {
			ShooterGameState->CurConnections = NumOfPlayers;
		}
	}
}
