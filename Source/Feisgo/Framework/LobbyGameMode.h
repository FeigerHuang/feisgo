// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "ShooterGameInstance.h"
#include "ShooterGameState.h"
#include "GameFramework/GameMode.h"
#include "LobbyGameMode.generated.h"

/**
 * GameMode just exist in server;
 */
UCLASS()
class FEISGO_API ALobbyGameMode : public AGameMode
{
	GENERATED_BODY()
public:
	void InitConnectionNumber();

	virtual void PostLogin(APlayerController* NewPlayer) override;

	virtual void Logout(AController* Exiting) override;

	virtual void BeginPlay() override;

	UFUNCTION(BlueprintCallable)
	void  SeamlessServerTravel();

	virtual void StartToLeaveMap() override;

	UFUNCTION(BlueprintImplementableEvent)
	void OnSeamlessTravelEnd();
private:
	UPROPERTY(VisibleAnywhere)
	int32 MaxPlayerNumber;

	UPROPERTY()
	AShooterGameState* ShooterGameState;
};
