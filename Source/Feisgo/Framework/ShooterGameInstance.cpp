// Fill out your copyright notice in the Description page of Project Settings.


#include "ShooterGameInstance.h"
#include "GameFramework/PlayerState.h"


void UShooterGameInstance::UpdatePlayerName()
{
	APlayerController* PlayerController = GetFirstLocalPlayerController();
	if (PlayerController) {
		APlayerState* PlayerState = PlayerController->GetPlayerState<APlayerState>();
		if (PlayerState) {
			PlayerState->SetPlayerName(ShooterName);
		}
	}
	if (GEngine) {
		GEngine->AddOnScreenDebugMessage(
			-1,
			10,
			FColor::Green,
			FString::Printf(TEXT("UpdateShooterName=%s"), *ShooterName)
			);
	}
}

void UShooterGameInstance::SetShooterName(FString InName)
{
	ShooterName = InName;
	UpdatePlayerName();
}

void UShooterGameInstance::SetMaxPlayerNumber(int32 InNumber)
{
	if (InNumber <= 0) {
		UE_LOG(LogTemp, Warning, TEXT("MaxPlayerNumber Can't less than zero"));
		return;
	}
	this->MaxPlayerNumber = InNumber;
}
