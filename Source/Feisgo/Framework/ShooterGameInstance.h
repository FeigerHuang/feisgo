// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/GameInstance.h"
#include "ShooterGameInstance.generated.h"
/**
 * 
 */
UCLASS()
class FEISGO_API UShooterGameInstance : public UGameInstance
{
	GENERATED_BODY()
public:
	UFUNCTION()
	void UpdatePlayerName();

	UFUNCTION(BlueprintCallable)
	void SetShooterName(FString InName);

	UFUNCTION(BlueprintCallable)
	void SetMaxPlayerNumber(int32 InNumber);

	FORCEINLINE int32 GetMaxPlayerNumber() const {return MaxPlayerNumber;}
private:
	UPROPERTY(VisibleAnywhere)
	int32 MaxPlayerNumber = 1;
	
	UPROPERTY(VisibleAnywhere)
	FString ShooterName;
};
