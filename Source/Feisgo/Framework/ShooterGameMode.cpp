// Fill out your copyright notice in the Description page of Project Settings.


#include "ShooterGameMode.h"
#include "Feisgo/Character/ShooterState.h"
#include "Kismet/GameplayStatics.h"

namespace MatchState
{
	const FName Cooldown = FName(TEXT("Cooldown")); // Match duration has been reached. Display winner and Begin Cooldown Timer;
}

AShooterGameMode::AShooterGameMode()
{
	bDelayedStart = true;

}

void AShooterGameMode::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);

	if (MatchState == MatchState::WaitingToStart) {
		const float CountdownTime = GetWorld()->GetTimeSeconds() - LevelStartTime;
		if (CountdownTime >= WarmupTime) {
			StartMatch();
		}
	} else if (MatchState == MatchState::InProgress) {
		const float CountdownTime = GetWorld()->GetTimeSeconds() - LevelStartTime - WarmupTime;
		if (CountdownTime >= MatchTime) {
			SetMatchState(MatchState::Cooldown);
		}
	} else if (MatchState == MatchState::Cooldown) {
		const float CountdownTime = GetWorld()->GetTimeSeconds() - LevelStartTime - WarmupTime - MatchTime - CooldownTime;
		if (CountdownTime >= 0.f) {
			RestartGame();
		}
	}
}

void AShooterGameMode::OnMatchStateSet()
{
	Super::OnMatchStateSet();

	// Tell all Player Play State Changed;
	for (auto It = GetWorld()->GetControllerIterator(); It; ++It) {
		AShooterController* ShooterController = Cast<AShooterController>(*It);
		if (ShooterController) {
			ShooterController->OnMatchStateSet(GetMatchState());
		}
	}
	
}

void AShooterGameMode::BeginPlay()
{
	Super::BeginPlay();


}

void AShooterGameMode::EliminatePlayer(AShooterCharacter* ElimPlayer, AShooterController* VicitimController, AShooterController* AttackerController)
{
	// if we access into cooldown state, shooter will not be Eliminated;
	if (MatchState == MatchState::Cooldown) return;
	
	if (ElimPlayer) {
		ElimPlayer->Eliminated();
	}

	if (VicitimController && AttackerController) {
		AShooterState* VicitimState = VicitimController->GetPlayerState<AShooterState>();
		AShooterState* AttackerState = AttackerController->GetPlayerState<AShooterState>();
		if (VicitimState != AttackerState) {
			AttackerState->AddOneKill_NotifyServer();
		}
	}
}

void AShooterGameMode::RequestRespawn(AShooterCharacter* ElimPlayer, AShooterController* ElimController)
{
	if (ElimPlayer) {
		ElimPlayer->Reset();
		FString StartTag = FString::Printf(TEXT("Start%d"), StartCount);
		StartCount = (StartCount + 1) % 7;
		AActor* Start = FindPlayerStart(ElimController, StartTag);
		if (Start) {
			RestartPlayerAtPlayerStart(ElimController, Start);
			ElimPlayer->IntoUnbeatableTime();
			if (GetWorld() && DefaultWeaponClass) {
				GetWorld()->SpawnActor<AWeapon>(DefaultWeaponClass, Start->GetActorLocation(), FRotator::ZeroRotator);
			}
		}
	}
}

void AShooterGameMode::PostSeamlessTravel()
{
	Super::PostSeamlessTravel();

	LevelStartTime = GetWorld()->GetTimeSeconds();
}
