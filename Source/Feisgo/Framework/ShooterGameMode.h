// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Feisgo/Character/ShooterCharacter.h"
#include "GameFramework/GameMode.h"
#include "ShooterGameMode.generated.h"

namespace MatchState
{
	extern FEISGO_API const FName Cooldown; // Match duration has been reached. Display winner and Begin Cooldown Timer;
}

/*
 * GameMode 仅存在Server端, 因此GameMode的函数调用发生在Server;
 */

UCLASS()
class FEISGO_API AShooterGameMode : public AGameMode
{
	GENERATED_BODY()
public:

	AShooterGameMode();
	
	virtual void Tick(float DeltaSeconds) override;

	virtual void OnMatchStateSet() override;
	
	virtual void EliminatePlayer(AShooterCharacter* ElimPlayer, AShooterController* VicitimController, AShooterController* AttackerController);

	virtual void RequestRespawn(AShooterCharacter* ElimPlayer, AShooterController* ElimController);
	
	virtual void PostSeamlessTravel() override;	
protected:
	virtual void BeginPlay() override;
    
    UPROPERTY(EditAnywhere)
	TSubclassOf<AWeapon> DefaultWeaponClass;
private:
	UPROPERTY(EditDefaultsOnly)
	float WarmupTime = 10.f;

	UPROPERTY(EditDefaultsOnly)
	float MatchTime = 10.f;

	UPROPERTY(EditDefaultsOnly)
	float CooldownTime = 10.f;
	
	float LevelStartTime = 0.f;

	int32 StartCount = 1;
public:
	FORCEINLINE float GetWarmupTime() const {return WarmupTime;}
	FORCEINLINE float GetMatchTime() const {return MatchTime;}
	FORCEINLINE float GetLevelStartTime() const {return LevelStartTime;}
	FORCEINLINE float GetCooldownTime() const {return CooldownTime;}
};

