// Fill out your copyright notice in the Description page of Project Settings.


#include "ShooterGameState.h"

#include "Net/UnrealNetwork.h"

void AShooterGameState::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
	DOREPLIFETIME(ThisClass, RankArray);
	DOREPLIFETIME(ThisClass, MaxConnections);
	DOREPLIFETIME(ThisClass, CurConnections);
}

void AShooterGameState::GetRankInfo(TArray<AShooterState*>& OutResult) const
{
	for (auto it = PlayerArray.begin(); it != PlayerArray.end(); ++it) {
		AShooterState* ShooterState = Cast<AShooterState>(*it);
		OutResult.AddUnique(ShooterState);
	}
	Algo::Sort(OutResult, [](AShooterState* A, AShooterState* B)->bool {
		return A->GetKillAmount() > B->GetKillAmount();
	});
	return;
}

