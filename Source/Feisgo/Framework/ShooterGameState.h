// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Feisgo/Character/ShooterState.h"
#include "GameFramework/GameState.h"
#include "ShooterGameState.generated.h"

/**
 * 
 */
UCLASS()
class FEISGO_API AShooterGameState : public AGameState
{
	GENERATED_BODY()

public:
	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;
	
	UPROPERTY(Replicated)
	TArray<AShooterState*> RankArray;
		
	void GetRankInfo(TArray<AShooterState*>& OutResult) const;
	
	UPROPERTY(Replicated, BlueprintReadOnly)
	int32 MaxConnections;

	UPROPERTY(Replicated, BlueprintReadOnly)
	int32 CurConnections;
};
