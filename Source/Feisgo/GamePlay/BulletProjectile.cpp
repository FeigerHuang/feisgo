// Fill out your copyright notice in the Description page of Project Settings.


#include "BulletProjectile.h"

#include "DrawDebugHelpers.h"
#include "Feisgo/Character/ShooterCharacter.h"
#include "GameFramework/Character.h"
#include "Kismet/GameplayStatics.h"

ABulletProjectile::ABulletProjectile()
{
	PrimaryActorTick.bCanEverTick = true;
	bReplicates = true;

	if (CollisionBox) {
		CollisionBox->OnComponentHit.RemoveAll(this);
		CollisionBox->SetCollisionEnabled(ECollisionEnabled::PhysicsOnly);
		CollisionBox->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Overlap);
	}
}

void ABulletProjectile::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	// check out tracer line just in server;

	TraceTrajectory();
}

void ABulletProjectile::BeginPlay()
{
	Super::BeginPlay();

	this->EndLocation = GetActorLocation();

}

void ABulletProjectile::Destroyed()
{
	AShooterCharacter* ShooterCharacter = Cast<AShooterCharacter>(GetOwner());
	if (HitEnemy) {
		AController* DamageInstigator = ShooterCharacter->Controller;
		UGameplayStatics::ApplyDamage(HitEnemy, BaseDamage, DamageInstigator, this, UDamageType::StaticClass());
	}
	Super::Destroyed();
}

void ABulletProjectile::TraceTrajectory()
{
	if (bImpacted) return;
	
	BeginLocation = EndLocation;
	EndLocation = GetActorLocation();
	
	const UWorld* World = GetWorld();
	if (World) {
		FHitResult HitResult;
		FCollisionObjectQueryParams ObjectQueryParams;
		ObjectQueryParams.AddObjectTypesToQuery(ECollisionChannel::ECC_Visibility);
		ObjectQueryParams.AddObjectTypesToQuery(ECollisionChannel::ECC_PhysicsBody);
		ObjectQueryParams.AddObjectTypesToQuery(ECollisionChannel::ECC_WorldStatic);
			
		FCollisionQueryParams QueryParams;
		QueryParams.bTraceComplex = false;
		AShooterCharacter* ShooterCharacter = Cast<AShooterCharacter>(GetOwner());
		if (ShooterCharacter) {
			QueryParams.AddIgnoredActor(ShooterCharacter->GetUniqueID());
		}
		World->LineTraceSingleByObjectType(HitResult, BeginLocation, EndLocation, ObjectQueryParams, QueryParams);

		if (HitResult.bBlockingHit) {
			AShooterCharacter* Enemy = Cast<AShooterCharacter>(HitResult.Actor);
			if (Enemy && HasAuthority()) {
				AController* DamageInstigator = ShooterCharacter->Controller;
				UGameplayStatics::ApplyDamage(Enemy, BaseDamage, DamageInstigator, this, UDamageType::StaticClass());
				UE_LOG(LogTemp, Warning, TEXT("Hit Target=%s"), *Enemy->GetName());
			}
			// DrawDebugSphere(World, HitResult.ImpactPoint, 5, 12, FColor::Cyan, false, 2.f);
			HitLocation = HitResult.ImpactPoint;
			bImpacted = true;
			// Server Need do this again;
			OnRep_HitLocation();
		}
	}
}

