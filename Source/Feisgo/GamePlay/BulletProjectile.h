// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Projectile.h"
#include "BulletProjectile.generated.h"

/**
 * 
 */
UCLASS()
class FEISGO_API ABulletProjectile : public AProjectile
{
	GENERATED_BODY()
	
public:
	ABulletProjectile();
	
	virtual void Tick(float DeltaTime) override;

	virtual void Destroyed() override;
protected:
	virtual void BeginPlay() override;

	/** this Overlap just Happened in the server*/
	void TraceTrajectory();
private:

	FVector BeginLocation;

	FVector EndLocation;
};

