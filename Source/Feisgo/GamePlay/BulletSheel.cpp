// Fill out your copyright notice in the Description page of Project Settings.


#include "BulletSheel.h"

#include "Kismet/GameplayStatics.h"

ABulletSheel::ABulletSheel()
{
	PrimaryActorTick.bCanEverTick = false;
	SheelMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("SheelMeshComp"));
	SetRootComponent(SheelMesh);
	
	SheelMesh->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
	SheelMesh->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Block);
	SheelMesh->SetCollisionResponseToChannel(ECollisionChannel::ECC_Camera, ECollisionResponse::ECR_Ignore);
	SheelMesh->SetCollisionResponseToChannel(ECollisionChannel::ECC_WorldDynamic, ECollisionResponse::ECR_Ignore);
	SheelMesh->SetCollisionResponseToChannel(ECollisionChannel::ECC_Pawn, ECollisionResponse::ECR_Ignore);
		
	SheelMesh->SetSimulatePhysics(true);
	SheelMesh->SetEnableGravity(true);
	SheelMesh->SetNotifyRigidBodyCollision(true);
	ShellEjectionImpulse = 8.f;
}

void ABulletSheel::DestroySelf()
{
	Destroy();
}

void ABulletSheel::BeginPlay()
{
	Super::BeginPlay();
	FVector InitDir = GetActorForwardVector();
	SheelMesh->AddImpulse(InitDir * ShellEjectionImpulse);
	FRotator RadomRotation;
	RadomRotation.Pitch = FMath::FRandRange(-10, 10);
	SheelMesh->SetRelativeRotation(RadomRotation);
	SheelMesh->OnComponentHit.AddDynamic(this, &ThisClass::OnHit);
	GetWorldTimerManager().SetTimer(DestroyTimeHandle, this, &ABulletSheel::DestroySelf, 5, false);
}

void ABulletSheel::OnHit(UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{
	if (bPlaying) return;
	
	if (SheelSound) {
		bPlaying = true;
		UGameplayStatics::PlaySoundAtLocation(
			this,
			SheelSound,
			GetActorLocation()
		);
	}

}


