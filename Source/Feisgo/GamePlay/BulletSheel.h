// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Sound/SoundCue.h"
#include "BulletSheel.generated.h"

UCLASS()
class FEISGO_API ABulletSheel : public AActor
{
	GENERATED_BODY()
	
public:	
	ABulletSheel();

	FTimerHandle DestroyTimeHandle;

	void DestroySelf();
protected:
	virtual void BeginPlay() override;

	UFUNCTION()
	void OnHit(UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit);

private:
	UPROPERTY(EditAnywhere)
	UStaticMeshComponent* SheelMesh;

	UPROPERTY(EditAnywhere)
	USoundCue* SheelSound;

	UPROPERTY(EditAnywhere)
	float ShellEjectionImpulse;

	bool bPlaying = false;
};
