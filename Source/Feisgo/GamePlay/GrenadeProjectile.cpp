// Fill out your copyright notice in the Description page of Project Settings.


#include "GrenadeProjectile.h"

#include "Kismet/GameplayStatics.h"

AGrenadeProjectile::AGrenadeProjectile()
{
	GrenadeMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("StaticMeshComp"));
	GrenadeMesh->SetupAttachment(GetRootComponent());
	GrenadeMesh->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	CollisionBox->SetCollisionResponseToChannel(ECollisionChannel::ECC_PhysicsBody, ECollisionResponse::ECR_Ignore);
	
	ProjectileMovementComponent->bShouldBounce = true;
}

void AGrenadeProjectile::BeginPlay()
{
	Super::BeginPlay();
}

void AGrenadeProjectile::OnBulletHit(UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{
	if (HitSoundCue) {
		UGameplayStatics::PlaySoundAtLocation(
			this,
			HitSoundCue,
			GetActorLocation()
		);
	}
}

void AGrenadeProjectile::Destroyed()
{
	PlayHitEffectAtPoint(GetActorLocation());

	if (HasAuthority()) {
		ApplyExplodeDamage();
	}
	
	Super::Destroyed();
	
}
