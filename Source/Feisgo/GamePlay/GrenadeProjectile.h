// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Projectile.h"
#include "GrenadeProjectile.generated.h"

/**
 * 
 */
UCLASS()
class FEISGO_API AGrenadeProjectile : public AProjectile
{
	GENERATED_BODY()
public:
	AGrenadeProjectile();	

protected:
	virtual void BeginPlay() override;
	
	virtual void OnBulletHit(UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit) override;

	virtual void Destroyed() override;

	UPROPERTY(EditAnywhere, Category="Grenade")
	UStaticMeshComponent* GrenadeMesh;

	UPROPERTY(EditAnywhere, Category="Grenade")
	USoundCue* HitSoundCue;
};
