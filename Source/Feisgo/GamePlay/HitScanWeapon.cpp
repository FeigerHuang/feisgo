// Fill out your copyright notice in the Description page of Project Settings.


#include "HitScanWeapon.h"

#include "Engine/SkeletalMeshSocket.h"
#include "Feisgo/Character/ShooterCharacter.h"
#include "Kismet/GameplayStatics.h"

void AHitScanWeapon::WeaponFire(const FVector& HitPosition)
{
	Super::WeaponFire(HitPosition);
	AShooterCharacter* Shooter = GetOwner<AShooterCharacter>();
	
	if (GetSkeletalMesh() == nullptr || Shooter == nullptr) return;

	AController* InstigatorController = Shooter->Controller;
	
	UWorld* World = GetWorld();

	const USkeletalMeshSocket* MuzzleSocket = GetSkeletalMesh()->GetSocketByName(FName("MuzzleFlash"));

	if (MuzzleSocket) {
		FVector Start = MuzzleSocket->GetSocketLocation(GetSkeletalMesh());
		FVector End =  Start + (HitPosition - Start) * 1.2f;

		FHitResult HitResult;
		FCollisionQueryParams QueryParams;
		QueryParams.AddIgnoredActor(Shooter);
		World->LineTraceSingleByChannel(HitResult, Start, End, ECollisionChannel::ECC_Visibility, QueryParams);
		if (HitResult.bBlockingHit) {
			// Damage just judge in the server;
			if (HasAuthority()) {
				AShooterCharacter* HitShooter = Cast<AShooterCharacter>(HitResult.GetActor());
				if (HitShooter) {
					UGameplayStatics::ApplyDamage(HitShooter, BaseDamage, InstigatorController, Shooter, UDamageType::StaticClass());
				}
			}

			if (ImpactParticle) {
				UGameplayStatics::SpawnEmitterAtLocation(
					World,
					ImpactParticle,
					HitResult.ImpactPoint,
					HitResult.ImpactNormal.Rotation()
					);
			}

			if (ImpactSound) {
				UGameplayStatics::SpawnSoundAtLocation(
					World,
					ImpactSound,
					HitResult.ImpactPoint
					);
			}
		}
	}
	
}
