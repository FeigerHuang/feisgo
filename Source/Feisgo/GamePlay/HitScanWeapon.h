// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Weapon.h"

#include "HitScanWeapon.generated.h"

/**
 * 
 */
UCLASS()
class FEISGO_API AHitScanWeapon : public AWeapon
{
	GENERATED_BODY()
public:

	virtual void WeaponFire(const FVector& HitPosition) override;
private:
	UPROPERTY(EditAnywhere)
	float BaseDamage = 20.f;

	UPROPERTY(EditAnywhere)
	UParticleSystem* ImpactParticle;

	UPROPERTY(EditAnywhere)
	USoundCue* ImpactSound;
};
