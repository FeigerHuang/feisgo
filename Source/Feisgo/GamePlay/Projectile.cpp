// Fill out your copyright notice in the Description page of Project Settings.


#include "Projectile.h"
#include "Components/BillboardComponent.h"
#include "Feisgo/Character/ShooterCharacter.h"
#include "Kismet/GameplayStatics.h"
#include "Net/UnrealNetwork.h"

AProjectile::AProjectile()
{
	PrimaryActorTick.bCanEverTick = true;
	bReplicates = true;
	SetReplicatingMovement(true);
	CollisionBox = CreateDefaultSubobject<UBoxComponent>(TEXT("CollisionBox"));
	SetRootComponent(CollisionBox);

	CollisionBox->SetCollisionObjectType(ECollisionChannel::ECC_WorldDynamic);
	CollisionBox->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
	CollisionBox->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Ignore);
	CollisionBox->SetCollisionResponseToChannel(ECollisionChannel::ECC_Visibility, ECollisionResponse::ECR_Block);
	CollisionBox->SetCollisionResponseToChannel(ECollisionChannel::ECC_WorldStatic, ECollisionResponse::ECR_Block);
	CollisionBox->SetCollisionResponseToChannel(ECollisionChannel::ECC_PhysicsBody, ECollisionResponse::ECR_Block);

	ProjectileMovementComponent = CreateDefaultSubobject<UProjectileMovementComponent>(TEXT("ProjectileMovementComp"));
	ProjectileMovementComponent->SetIsReplicated(true);
	ProjectileMovementComponent->bRotationFollowsVelocity = true;
	ProjectileMovementComponent->ProjectileGravityScale = 0.f;
	
}

void AProjectile::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME_CONDITION(ThisClass, HitLocation, COND_Never);
}


void AProjectile::BeginPlay()
{
	
	Super::BeginPlay();

	CollisionBox->OnComponentHit.AddDynamic(this, &ThisClass::OnBulletHit);
	
	if (Tracer) {
		TracerComponent = UGameplayStatics::SpawnEmitterAttached(
			Tracer,
			CollisionBox,
			FName(),
			GetActorLocation(),
			GetActorRotation(),
			EAttachLocation::KeepWorldPosition
			);
	}

	SetLifeSpan(LifeTime);
}

void AProjectile::OnBulletHit(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& HitResult)
{
	if (bImpacted) return;
	
	AShooterCharacter* Enemy = Cast<AShooterCharacter>(OtherActor);
	AShooterCharacter* OwnerShooter = Cast<AShooterCharacter>(GetOwner());
	if (Enemy == OwnerShooter) return;
	
	if (Enemy) {
		SetHitEnemy(Enemy);
	}
	
	HitLocation = HitResult.ImpactPoint;

	bImpacted = true;

	OnRep_HitLocation();
}

void AProjectile::PlayHitEffectAtPoint(FVector InPoint)
{
		
	if (HitParticle) {
		UGameplayStatics::SpawnEmitterAtLocation(
			this,
			HitParticle,
			InPoint	
			);
	}
	if (HitSound) {
		UGameplayStatics::PlaySoundAtLocation(
			this,
			HitSound,
				InPoint
		);
	}
}


void AProjectile::OnRep_HitLocation() 
{
	PlayHitEffectAtPoint(HitLocation);

	// We need Ensure finish Play Effect;
	SetLifeSpan(1.f);
}

void AProjectile::ApplyExplodeDamage()
{
	ACharacter* OwnerCharacter = GetOwner<ACharacter>();

	if (OwnerCharacter == nullptr) return;
	
	AController* InstigateController = OwnerCharacter->GetController();

	if (OwnerCharacter && InstigateController) {
		UGameplayStatics::ApplyRadialDamageWithFalloff(
			this,
			BaseDamage,
			MinDamage,
			GetActorLocation(),
			ExplodeInnerRadius,
			ExplodeOuterRadius,
			1,
			UDamageType::StaticClass(),
			TArray<AActor*>(),
			this, //DamageCauser
			InstigateController,
			ECollisionChannel::ECC_PhysicsBody
		);
	}
}

// Destroyed will call on all machine destroy the Actor;
void AProjectile::Destroyed()
{
	Super::Destroyed();
}


void AProjectile::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

