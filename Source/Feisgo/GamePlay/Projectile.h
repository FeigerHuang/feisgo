// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/BoxComponent.h"
#include "GameFramework/Actor.h"
#include "GameFramework/ProjectileMovementComponent.h"
#include "Sound/SoundCue.h"
#include "Projectile.generated.h"

class AShooterCharacter;
UCLASS()
class FEISGO_API AProjectile : public AActor
{
	GENERATED_BODY()
	
public:	
	AProjectile();

	virtual void Tick(float DeltaTime) override;

	virtual void Destroyed() override;

	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;

	FORCEINLINE void SetHitEnemy(AShooterCharacter* Enemy) {HitEnemy = Enemy;}

protected:
	virtual void BeginPlay() override;

	UFUNCTION()
	virtual void OnBulletHit(UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit);

	void PlayHitEffectAtPoint(FVector InPoint);
	
	UPROPERTY(EditAnywhere)
	UBoxComponent* CollisionBox;

	UPROPERTY(VisibleAnywhere)
	UProjectileMovementComponent* ProjectileMovementComponent;
	
	UPROPERTY(EditAnywhere)
	UParticleSystem* Tracer;

	UParticleSystemComponent* TracerComponent;

	UPROPERTY(EditAnywhere)
	UParticleSystem* HitParticle;

	UPROPERTY(EditAnywhere)
	USoundCue* HitSound;

	UPROPERTY(ReplicatedUsing = OnRep_HitLocation)
	FVector HitLocation;

	UFUNCTION()
	void OnRep_HitLocation();
	
	UPROPERTY(VisibleAnywhere)
	AShooterCharacter* HitEnemy;

	bool bImpacted = false;

	UPROPERTY(EditDefaultsOnly)
	float LifeTime = 5.f;

	UPROPERTY(EditAnywhere, Category = Damage)
	float BaseDamage = 20.f;

	UPROPERTY(EditAnywhere, Category = Damage)
	float MinDamage = 20.f;

	UPROPERTY(EditAnywhere, Category = Damage)
	float ExplodeInnerRadius = 200;
	
	UPROPERTY(EditAnywhere, Category = Damage)
	float ExplodeOuterRadius = 600;
	
	void ApplyExplodeDamage();
};


