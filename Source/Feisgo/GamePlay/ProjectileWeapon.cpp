// Fill out your copyright notice in the Description page of Project Settings.


#include "ProjectileWeapon.h"

#include "Feisgo/Character/ShooterCharacter.h"
#include "Kismet/GameplayStatics.h"

AProjectileWeapon::AProjectileWeapon()
{
	bReplicates = true;	
}

void AProjectileWeapon::WeaponFire(const FVector& HitPosition)
{
	Super::WeaponFire(HitPosition);

	// Projectile Just Spawn in the server, replicate to the Client;
	if (!HasAuthority()) return;
	
	APawn* InstigaterPawn = Cast<APawn>(GetOwner());
	if (InstigaterPawn == nullptr) {
		UE_LOG(LogTemp, Warning, TEXT("Weapon Can't Fire at no Owner!"));
		return;
	}
	
	if (ProjectileClass && GetSkeletalMesh()) {
		FVector SpawnLocation = GetSkeletalMesh()->GetSocketLocation("MuzzleFlash");
		// get the vec Muzzle to the Hit point;
		FVector TargetVector = HitPosition - SpawnLocation;
		FRotator SpawnRotation = TargetVector.Rotation();
		
		FActorSpawnParameters SpawnParameters;
		SpawnParameters.Owner = GetOwner();
		SpawnParameters.Instigator = InstigaterPawn;
		GetWorld()->SpawnActor<AProjectile>(ProjectileClass, SpawnLocation, SpawnRotation, SpawnParameters);
	}
	
}

