// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Weapon.h"
#include "ProjectileWeapon.generated.h"

/**
 * 
 */
UCLASS()
class FEISGO_API AProjectileWeapon : public AWeapon
{
	GENERATED_BODY()
public:
	AProjectileWeapon();

	virtual void WeaponFire(const FVector& HitPosition) override;

protected:

	UPROPERTY(EditAnywhere)
	TSubclassOf<AProjectile> ProjectileClass;

};
