// Fill out your copyright notice in the Description page of Project Settings.


#include "RespawnArea.h"

#include "Feisgo/Character/ShooterCharacter.h"
#include "Feisgo/Framework/ShooterGameMode.h"
#include "Kismet/GameplayStatics.h"

// Sets default values
ARespawnArea::ARespawnArea()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	RespawnBox = CreateDefaultSubobject<UBoxComponent>(TEXT("RespawnBox"));
	SetRootComponent(RespawnBox);
	
	if (HasAuthority()) {
		RespawnBox->OnComponentBeginOverlap.AddDynamic(this, &ThisClass::OnBoxBeginOverlap);
	}
}

// Called when the game starts or when spawned
void ARespawnArea::BeginPlay()
{
	Super::BeginPlay();
	
}

void ARespawnArea::OnBoxBeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	AShooterCharacter* Shooter = Cast<AShooterCharacter>(OtherActor);
	if (Shooter) {
		AShooterController* ShooterController = Cast<AShooterController>(Shooter->Controller);
		AShooterGameMode* GameMode = Cast<AShooterGameMode>(UGameplayStatics::GetGameMode(this));
		if (GameMode && ShooterController) {
			GameMode->EliminatePlayer(Shooter, ShooterController, ShooterController);
		}
	}
}

// Called every frame
void ARespawnArea::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

