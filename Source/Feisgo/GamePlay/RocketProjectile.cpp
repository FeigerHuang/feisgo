// Fill out your copyright notice in the Description page of Project Settings.


#include "RocketProjectile.h"

#include "Components/AudioComponent.h"
#include "Feisgo/Character/ShooterCharacter.h"
#include "GameFramework/Character.h"
#include "Kismet/GameplayStatics.h"

ARocketProjectile::ARocketProjectile()
{
	RocketStaticMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("StaticMeshComp"));
	RocketStaticMesh->SetupAttachment(CollisionBox);
}

void ARocketProjectile::BeginPlay()
{
	Super::BeginPlay();

	if (ProjectileLoop && LoopingSoundAttenuation) {
		ProjectileLoopComp = UGameplayStatics::SpawnSoundAttached(
			ProjectileLoop,
			GetRootComponent(),
			FName(),
			GetActorLocation(),
			EAttachLocation::KeepWorldPosition,
			false,
			1.0f,
			1.0f,
			0.f,
			LoopingSoundAttenuation
			);
	}
}

void ARocketProjectile::OnBulletHit(UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& HitResult)
{
	if (bImpacted) return;

	AShooterCharacter* Enemy = Cast<AShooterCharacter>(OtherActor);
	AShooterCharacter* OwnerShooter = Cast<AShooterCharacter>(GetOwner());
	if (Enemy == OwnerShooter) return;

	if (Enemy) {
		SetHitEnemy(Enemy);
	}

	HitLocation = HitResult.ImpactPoint;

	bImpacted = true;

	if (HasAuthority()) {
		ApplyExplodeDamage();

		Destroy();
	}
}

void ARocketProjectile::Destroyed()
{
	
	PlayHitEffectAtPoint(HitLocation);
	
	if (ProjectileLoopComp && ProjectileLoopComp->IsPlaying()) {
		ProjectileLoopComp->Stop();
	}
	
	Super::Destroyed();
}
