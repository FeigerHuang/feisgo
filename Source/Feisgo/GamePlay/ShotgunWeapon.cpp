// Fill out your copyright notice in the Description page of Project Settings.


#include "ShotgunWeapon.h"
#include "DrawDebugHelpers.h"
#include "Feisgo/Character/ShooterCharacter.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetMathLibrary.h"

void AShotgunWeapon::WeaponFire(const FVector& HitPosition)
{
	Super::WeaponFire(HitPosition);
	
	APawn* OwnerPawn = GetOwner<APawn>();
	AController* InstigatorController = OwnerPawn->GetController();
	
	if (GetSkeletalMesh() == nullptr || OwnerPawn == nullptr) return;
	FVector TraceStart = GetSkeletalMesh()->GetSocketLocation(FName("MuzzleFlash"));

	for (uint32 i = 0; i < NumberOfPellets; ++i) {
		FVector RandEndPoint = TraceEndWithScatter(TraceStart, HitPosition);

		FHitResult HitResult;
		GetWorld()->LineTraceSingleByChannel(
			HitResult,
			TraceStart,
			RandEndPoint,
			ECollisionChannel::ECC_Visibility
			);

		if (HitResult.bBlockingHit) {
			AShooterCharacter* HitShooter = Cast<AShooterCharacter>(HitResult.GetActor());
			if (HasAuthority() && HitShooter && InstigatorController) {
				UGameplayStatics::ApplyDamage(
					HitShooter,
					BaseDamage,
					InstigatorController,
					OwnerPawn,
					UDamageType::StaticClass()
					);
			}
			if (ImpactParticle) {
				UGameplayStatics::SpawnEmitterAtLocation(
					this,
					ImpactParticle,
					HitResult.ImpactPoint);
			}	
		}
	}
}

FVector AShotgunWeapon::TraceEndWithScatter(const FVector& TraceStart, const FVector& HitTarget)
{

	FVector TraceDirection = (HitTarget - TraceStart).GetSafeNormal();
	FVector SphereLoc = TraceStart + TraceDirection * DistanceToSphere;

	FVector RandVec = UKismetMathLibrary::RandomUnitVector() * FMath::FRandRange(0.f, SphereRadius);
	FVector EndLoc = SphereLoc + RandVec;
	FVector ToEndLoc = EndLoc - TraceStart;

	// DrawDebugSphere(GetWorld(), SphereLoc, SphereRadius, 12, FColor::Red, true);
	// DrawDebugSphere(GetWorld(), EndLoc, 4.f, 12, FColor::Orange, true);
	// DrawDebugLine(GetWorld(), TraceStart, EndLoc, FColor::Cyan, true);
	
	return FVector(TraceStart + ToEndLoc.GetSafeNormal() * TRACE_LEN);
}
