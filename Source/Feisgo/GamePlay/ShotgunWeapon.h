// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Weapon.h"
#include "ShotgunWeapon.generated.h"

/**
 * 
 */
UCLASS()
class FEISGO_API AShotgunWeapon : public AWeapon
{
	GENERATED_BODY()
public:
	virtual void WeaponFire(const FVector& HitPosition) override;
protected:

	/**
	 * Trace and Scatter 
	 */
	auto TraceEndWithScatter(const FVector& TraceStart, const FVector& HitTarget) -> FVector;	

	UPROPERTY(EditAnywhere, Category="Weapon Scatter")
	uint32 NumberOfPellets = 10;

	UPROPERTY(EditAnywhere, Category="Weapon Scatter")
	float DistanceToSphere = 500.f;

	UPROPERTY(EditAnywhere, Category="Weapon Scatter")
	float SphereRadius = 75.f;
	
	UPROPERTY(EditAnywhere)
	UParticleSystem* ImpactParticle;

	UPROPERTY(EditAnywhere)
	float BaseDamage = 15.f;
};
