// Fill out your copyright notice in the Description page of Project Settings.


#include "SpawnBox.h"


// Sets default values
ASpawnBox::ASpawnBox()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

	SpawnBox = CreateDefaultSubobject<UBoxComponent>(TEXT("BoxComp"));
	
	SetRootComponent(SpawnBox);
	
}

void ASpawnBox::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called when the game starts or when spawned
void ASpawnBox::BeginPlay()
{
	Super::BeginPlay();

	if (HasAuthority()) {
		Spawn();
	}
}

void ASpawnBox::OnSpawnTimerEnd()
{
	if (SpawnActor == nullptr || SpawnActor->IsPendingKill()) {
		GetWorldTimerManager().SetTimer(SpawnTimerHandle, this, &ThisClass::Spawn, SpawnRate);
	} else {
		GetWorldTimerManager().SetTimer(SpawnTimerHandle, this, &ThisClass::OnSpawnTimerEnd, SpawnRate);
	}
}

void ASpawnBox::Spawn()
{
	if (HasAuthority()) {
		int32 LastIndex = SpawnClassArray.Num() - 1;
		int32 RandomIndex = FMath::RandRange(0, LastIndex);
		SpawnActor = GetWorld()->SpawnActor<AActor>(SpawnClassArray[RandomIndex], GetActorLocation(), FRotator::ZeroRotator);
		OnSpawnTimerEnd();
	}
}
