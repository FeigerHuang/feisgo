// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/BoxComponent.h"
#include "GameFramework/Actor.h"
#include "SpawnBox.generated.h"

UCLASS()
class FEISGO_API ASpawnBox : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ASpawnBox();

	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UPROPERTY(EditAnywhere)
	UBoxComponent* SpawnBox;
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(EditAnywhere, Category=SpawnActorInfo)
	TArray<TSubclassOf<AActor>> SpawnClassArray;

	UPROPERTY(VisibleAnywhere,Category=SpawnActorInfo)
	AActor* SpawnActor;
	
	UPROPERTY(EditAnywhere, Category=SpawnActorInfo)
	float SpawnRate = 30.f;
	
	FTimerHandle SpawnTimerHandle;

	void OnSpawnTimerEnd();

	void Spawn();
};
