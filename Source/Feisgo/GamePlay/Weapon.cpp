// Fill out your copyright notice in the Description page of Project Settings.


#include "Weapon.h"

#include "Engine/SkeletalMeshSocket.h"
#include "Feisgo/Character/ShooterCharacter.h"
#include "GameFramework/Character.h"
#include "Kismet/GameplayStatics.h"
#include "Net/UnrealNetwork.h"

// Sets default values
AWeapon::AWeapon()
{
	PrimaryActorTick.bCanEverTick = false;
	bReplicates = true;
	SetReplicatingMovement(true);
	
	WeaponMesh = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("WeaponMesh"));
	SetRootComponent(WeaponMesh);

	DetectSphere = CreateDefaultSubobject<USphereComponent>(TEXT("DetectSphere"));
	DetectSphere->SetupAttachment(RootComponent);
	DetectSphere->SetSphereRadius(120.f);
		
	WeaponMesh->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Block);
	WeaponMesh->SetCollisionResponseToChannel(ECollisionChannel::ECC_Pawn, ECollisionResponse::ECR_Ignore);
	WeaponMesh->SetCollisionResponseToChannel(ECollisionChannel::ECC_Camera, ECollisionResponse::ECR_Ignore);
	WeaponMesh->SetCollisionResponseToChannel(ECollisionChannel::ECC_WorldDynamic, ECollisionResponse::ECR_Ignore);
	WeaponMesh->SetCollisionResponseToChannel(ECollisionChannel::ECC_PhysicsBody, ECollisionResponse::ECR_Ignore);
	WeaponMesh->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	WeaponMesh->SetCollisionObjectType(ECollisionChannel::ECC_WorldDynamic);
	
	DetectSphere->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Ignore);
	DetectSphere->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	
	PickupWidget = CreateDefaultSubobject<UWidgetComponent>(TEXT("PickUpWidget"));
	PickupWidget->SetupAttachment(RootComponent);
	PickupWidget->SetVisibility(false);

	WeaponMesh->SetRenderCustomDepth(true);
	WeaponMesh->SetCustomDepthStencilValue(CUSTOM_DEPTH_PURPLE);
	WeaponMesh->MarkRenderStateDirty();
	
}

// Called every frame
void AWeapon::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AWeapon::SetPickupVisible(bool bVisible)
{
	if (PickupWidget) {
		PickupWidget->SetVisibility(bVisible);
	}
}

void AWeapon::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
	DOREPLIFETIME(ThisClass, WeaponState);
	DOREPLIFETIME(ThisClass, Ammo);
}

void AWeapon::WeaponFire(const FVector& HitPosition)
{
	if (FireAnimation && WeaponMesh) {
		WeaponMesh->PlayAnimation(FireAnimation, false);

		if (BulletClass) {
			auto SocketTrans = WeaponMesh->GetSocketTransform(FName("AmmoEject"));
			GetWorld()->SpawnActor<ABulletSheel>(BulletClass, SocketTrans.GetLocation(), SocketTrans.Rotator());
		}
	}
	
	if (ShooterController && ShooterController->IsLocalController()) {
		if (FireCameraShake != nullptr) {
			ShooterController->ClientStartCameraShake(FireCameraShake, 1);
			ShooterController->AddPitchInput(-Recoil);
		}
	} 	
}

void AWeapon::OnFireBegin()
{
	Recoil = FMath::Clamp(Recoil + RecoilFactor, 0.f, 1.f);
}

void AWeapon::OnFireEnd()
{
	auto Lamda = [&] {
		Recoil = FMath::Clamp(RecoilFactor, 0.f, 1.f);
	};
	GetWorldTimerManager().SetTimer(RecoilTimerHandle, Lamda, 0.15f, false);
}

void AWeapon::SetVisible(bool bVisible) const
{
	if (GetSkeletalMesh() == nullptr) return;
	WeaponMesh->SetVisibility(bVisible);
}

// Called when the game starts or when spawned
void AWeapon::BeginPlay()
{
	Super::BeginPlay();

	// if run this code in server then open Detect sphere collision;
	if (GetLocalRole() == ROLE_Authority) {
		DetectSphere->SetCollisionResponseToChannel(ECollisionChannel::ECC_Pawn, ECollisionResponse::ECR_Overlap);
		DetectSphere->OnComponentBeginOverlap.AddDynamic(this, &ThisClass::OnDetectBeginOverlap);
		DetectSphere->OnComponentEndOverlap.AddDynamic(this, &ThisClass::OnDetectEndOverlap);
	}
}

// this Function only register on server;
void AWeapon::OnDetectBeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	AShooterCharacter* Shooter = Cast<AShooterCharacter>(OtherActor);
	if (IsValid(Shooter)) {
		Shooter->SetOverlapWeapon(this);
	}
}

// this Function only register on server;
void AWeapon::OnDetectEndOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	AShooterCharacter* Shooter = Cast<AShooterCharacter>(OtherActor);
	if (IsValid(Shooter)) {
		Shooter->SetOverlapWeapon(nullptr);
	}
}


void AWeapon::Server_SetWeaponState_Implementation(EWeaponState NewState)
{
		SetWeaponState(NewState);
}

void AWeapon::SetWeaponState(EWeaponState NewState)
{
	if (WeaponState != NewState) {
		WeaponState = NewState;
		// in listen server we should call On_rep by hands;
		OnRep_WeaponState();
	} 
}

void AWeapon::OnRep_WeaponState()
{
	switch (WeaponState) {
		case EWeaponState::EWS_Equiped:
			OnWeaponEquipped();
			break;
		case EWeaponState::EWS_Dropped:
			OnWeaponDropped();
			break;
		case EWeaponState::EWS_Knapsack:
			OnWeaponKnapsack();
			break;
		default:
		{
			UE_LOG(LogTemp, Warning, TEXT("Unknown Weapon State!"));
			break;
		}
	}
}

void AWeapon::OnWeaponEquipped()
{
	
	WeaponMesh->SetSimulatePhysics(false);
	WeaponMesh->SetEnableGravity(false);
	WeaponMesh->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	WeaponMesh->SetRenderCustomDepth(false);
	DetectSphere->SetCollisionResponseToChannel(ECollisionChannel::ECC_Pawn, ECollisionResponse::ECR_Ignore);
	// We must do this affter SetNoCollision;
	AttachToOwnerSocket(FName("RightHandSocket"));

	PlayEquipSound();
	SetHUDAmmo();
}

void AWeapon::OnWeaponDropped()
{
	
	if (GetLocalRole() == ROLE_Authority) {
		DetectSphere->SetCollisionResponseToChannel(ECollisionChannel::ECC_Pawn, ECollisionResponse::ECR_Overlap);
	}
	WeaponMesh->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
	WeaponMesh->SetSimulatePhysics(true);
	WeaponMesh->SetEnableGravity(true);
	WeaponMesh->SetRenderCustomDepth(true);
	WeaponMesh->SetVisibility(true);
	
	{
		if (ShooterController) {
			AShooterCharacter* Shooter = ShooterController->GetPawn<AShooterCharacter>();
			if (Shooter) {
				// 因为Sniper开镜时人物被隐藏, 因此在开镜丢武器时需要设置其可见
				if (CurrentWeaponType == EWeaponTypes::EWT_SniperRife && Shooter->IsLocallyControlled()) {
					Shooter->GetMesh()->SetVisibility(true);
					Shooter->SniperRifleZooming(false);
				}
			}
		}
	}
	ShooterController = nullptr;
	// Do this Again, 虽然可以不做;
	SetOwner(nullptr);
}

void AWeapon::OnWeaponKnapsack()
{
	WeaponMesh->SetSimulatePhysics(false);
	WeaponMesh->SetEnableGravity(false);
	WeaponMesh->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	WeaponMesh->SetRenderCustomDepth(false);
	DetectSphere->SetCollisionResponseToChannel(ECollisionChannel::ECC_Pawn, ECollisionResponse::ECR_Ignore);
	// We must do this affter SetNoCollision;
	AttachToOwnerSocket(FName("backpackSocket"));

	PlayEquipSound();
}

void AWeapon::AttachToOwnerSocket(FName SocketName)
{
	AShooterCharacter* Shooter = Cast<AShooterCharacter>(GetOwner());
	if (Shooter) {
		auto Socket = Shooter->GetMesh()->GetSocketByName(SocketName);
		Socket->AttachActor(this, Shooter->GetMesh());
		// then set owner Controller
		ShooterController = Shooter->GetController<AShooterController>();
	}
}

FVector AWeapon::GetMuzzleLocation() const
{
	if (GetSkeletalMesh()) {
		return GetSkeletalMesh()->GetSocketLocation(FName("MuzzleFlash"));
	}

	return FVector();
}

void AWeapon::OnRep_Ammo()
{
	if (CurrentWeaponType == EWeaponTypes::EWT_Shotgun) {
		AShooterCharacter* Shooter = GetOwner<AShooterCharacter>();
		if (Shooter) {
			Shooter->JumpToReloadEnd();
		}	
	}
	SetHUDAmmo();
}

void AWeapon::SpendRound()
{
	Ammo = FMath::Clamp(Ammo - 1, 0, ClipCapacity);
	SetHUDAmmo();	
}

void AWeapon::PlayEquipSound()
{
	if (EquipSoundCue) {
		UGameplayStatics::PlaySoundAtLocation(
			this,
			EquipSoundCue,
			GetActorLocation()
			);
	}
}

void AWeapon::SetHUDAmmo()
{
	if (ShooterController == nullptr) {
		AShooterCharacter* Shooter = GetOwner<AShooterCharacter>();
		if (Shooter == nullptr) return;
		ShooterController = Shooter->GetController<AShooterController>();
	}

	if (ShooterController && ShooterController->IsLocalController()) {
		ShooterController->SetHUDClipAmmo(Ammo);
		ShooterController->SetHUDWeaponUI(CurrentWeaponType);
	}
}

void AWeapon::IncreaseAmmo(int32 AmmoToAdd)
{
	// don't call this function in the client;
	Ammo = FMath::Clamp(Ammo + AmmoToAdd, 0, ClipCapacity);
	OnRep_Ammo();
}

