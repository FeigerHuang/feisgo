// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BulletSheel.h"
#include "Projectile.h"
#include "Components/SphereComponent.h"
#include "Components/WidgetComponent.h"
#include "Feisgo/Character/ShooterController.h"
#include "Feisgo/ShooterTypes/WeaponTypes.h"
#include "Feisgo/UI/ShooterHUD.h"
#include "GameFramework/Actor.h"
#include "Weapon.generated.h"

// Define a cup of state about weapon
UENUM(BlueprintType)
enum  class EWeaponState : uint8
{
	EWS_Initial
	UMETA(DisplayName="Weapon_Initial"),

	EWS_Equiped
	UMETA(DisplayName="Weapon_Equiped"),

	EWS_Dropped
	UMETA(DisplayName="Weapon_Dropped"),

	EWS_Knapsack
	UMETA(DisplayName="Weapon_Knapsack"),
	
	Default_MAX
	UMETA(DisplayName="Default_Max")
};

UCLASS()
class FEISGO_API AWeapon : public AActor
{
	GENERATED_BODY()
	friend class UCombatComponent;	
public:	
	AWeapon();

	virtual void Tick(float DeltaTime) override;

	void SetPickupVisible(bool bVisible);

	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;

	UFUNCTION(BlueprintCallable)
	FORCEINLINE USkeletalMeshComponent* GetSkeletalMesh() const {return WeaponMesh;};

	UFUNCTION(BlueprintCallable)
	virtual void WeaponFire(const FVector& HitPosition);

	UPROPERTY(EditAnywhere, Category = Crosshairs)
	FHUDPacket HUDPacket;
	
	FTimerHandle RecoilTimerHandle;

	void SetVisible(bool bVisible) const;
	
	/*
	 * Automatic Fire, and Fire Delay setting
	 */
	void OnFireBegin();

	void OnFireEnd();
	
	UPROPERTY(EditAnywhere, Category=Combat)
	float FireDelay = 0.15f;

	UPROPERTY(EditAnywhere, Category=Combat)
	bool bAutomaticFire = true;

	void AttachToOwnerSocket(FName SocketName);

	FVector GetMuzzleLocation() const;
protected:
	virtual void BeginPlay() override;

	UFUNCTION()
	virtual void OnDetectBeginOverlap(
		UPrimitiveComponent* OverlappedComponent,
		AActor* OtherActor,
		UPrimitiveComponent* OtherComp,
		int32 OtherBodyIndex,
		bool bFromSweep,
		const FHitResult & SweepResult
	);

	UFUNCTION()
	virtual void OnDetectEndOverlap(
		UPrimitiveComponent* OverlappedComponent,
		AActor* OtherActor,
		UPrimitiveComponent* OtherComp,
		int32 OtherBodyIndex
	);

	UFUNCTION(Server, Reliable)
	void Server_SetWeaponState(EWeaponState NewState);

	void SetWeaponState(EWeaponState NewState);

	// when the weapon state change we should set something
	UFUNCTION()
	virtual void OnRep_WeaponState();

	void OnWeaponEquipped();

	void OnWeaponDropped();

	void OnWeaponKnapsack();
	/**
	 * weapon Zoom in setting
	 */
	UPROPERTY(EditAnywhere, Category=Zoom)
	float ZoomFOV = 30.f;

	UPROPERTY(EditAnywhere, Category=Zoom)
	float ZoomSpeed = 20.f;

	UPROPERTY(EditDefaultsOnly, Category=Weapon, meta=(AllowPrivateAccess=true))
	EWeaponTypes CurrentWeaponType;

	UPROPERTY(EditDefaultsOnly, Category=Weapon)
	TSubclassOf<UMatineeCameraShake> FireCameraShake;
private:
	UPROPERTY(EditDefaultsOnly, meta=(AllowPrivateAccess=true), Category=Weapon)
	USoundCue* EquipSoundCue;
	
	UPROPERTY(EditDefaultsOnly,BlueprintReadOnly, meta=(AllowPrivateAccess=true), Category=Weapon)
	USkeletalMeshComponent* WeaponMesh;

	UPROPERTY(EditDefaultsOnly,BlueprintReadOnly, meta=(AllowPrivateAccess=true), Category=Weapon)
	USphereComponent* DetectSphere;

	UPROPERTY(ReplicatedUsing=OnRep_WeaponState,VisibleAnywhere, Category=Weapon)
	EWeaponState WeaponState;

	UPROPERTY(EditAnywhere, Category=Weapon)
	UWidgetComponent* PickupWidget;

	UPROPERTY(EditAnywhere, Category=Weapon)
	UAnimationAsset* FireAnimation;

	UPROPERTY(EditAnywhere, Category=Weapon)
	TSubclassOf<ABulletSheel> BulletClass;

	// The Recoil changed when we fired;
	UPROPERTY(EditAnywhere, Category=FireRecoil, meta=(ClampMin=0.f, ClampMax=1.f))
	float Recoil = 0.f;

	UPROPERTY(EditAnywhere, Category=FireRecoil)
	float RecoilFactor = 0.1f;

	/**
	 * Ammo relative
	 */
	UPROPERTY(EditAnywhere)
	int32 ClipCapacity = 30;
	
	UPROPERTY(ReplicatedUsing=OnRep_Ammo, EditAnywhere)
	int32 Ammo = 30;
	
	UFUNCTION()
	void OnRep_Ammo();

	void SpendRound();

	void PlayEquipSound();
	
	AShooterController* ShooterController;
public:
	UFUNCTION(BlueprintCallable)
	void SetHUDAmmo();
	
	FORCEINLINE float GetZoomFOV() const {return ZoomFOV;}
	FORCEINLINE float GetZoomSpeed() const {return ZoomSpeed;}
	FORCEINLINE float GetRocil() const {return Recoil;}
	FORCEINLINE bool ClipEmpty() const {return Ammo <= 0.f;}
	FORCEINLINE bool ClipFull() const {return Ammo == ClipCapacity;}
	FORCEINLINE EWeaponTypes GetWeaponType() const {return CurrentWeaponType;}
	FORCEINLINE int32 GetClipAmmo() const {return Ammo;}
	FORCEINLINE int32 GetClipCapacity() const {return ClipCapacity;}
	// Server called only
	void IncreaseAmmo(int32 AmmoToAdd);
};


