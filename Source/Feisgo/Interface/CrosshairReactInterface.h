// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "CrosshairReactInterface.generated.h"

// This class does not need to be modified.
UINTERFACE(MinimalAPI)
class UCrosshairReactInterface : public UInterface
{
	GENERATED_BODY()
};

/**
 * 
 */
class FEISGO_API ICrosshairReactInterface
{
	GENERATED_BODY()

	// Add interface functions to this class. This is the class that will be inherited to implement this interface.
public:
};
