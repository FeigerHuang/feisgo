// Fill out your copyright notice in the Description page of Project Settings.


#include "AmmoPickUp.h"

#include "Feisgo/Character/ShooterCharacter.h"

AAmmoPickUp::AAmmoPickUp()
{
	if (StaticMesh) {
		StaticMesh->bRenderCustomDepth = true;
		StaticMesh->SetCustomDepthStencilValue(CUSTOM_DEPTH_TAN);
		StaticMesh->MarkRenderStateDirty();
	}	
}

void AAmmoPickUp::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (bAutoRotate) {
		AddActorWorldRotation(FRotator(0.f, DeltaTime * RotateVelocity, 0.f));
	}
}

void AAmmoPickUp::OnPickSphereOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	Super::OnPickSphereOverlap(OverlappedComponent, OtherActor, OtherComp, OtherBodyIndex, bFromSweep, SweepResult);
	AShooterCharacter *Shooter = Cast<AShooterCharacter>(OtherActor);	
	if (HasAuthority() && Shooter) {
		Shooter->PickUpAmmo(WeaponType, AddAmmo);
		Destroy();
	}
}
