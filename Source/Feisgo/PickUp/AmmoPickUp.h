// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "PickUpBase.h"
#include "Feisgo/ShooterTypes/WeaponTypes.h"
#include "AmmoPickUp.generated.h"

/**
 * 
 */
UCLASS()
class FEISGO_API AAmmoPickUp : public APickUpBase
{
	GENERATED_BODY()
public:
	AAmmoPickUp();

	virtual void Tick(float DeltaTime) override;
protected:

	virtual void OnPickSphereOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult) override;

	UPROPERTY(EditAnywhere, Category=AmmoPickUp)
	float RotateVelocity = 30.f;
	
	UPROPERTY(EditAnywhere, Category=AmmoPickUp)
	bool bAutoRotate = true;

	UPROPERTY(EditAnywhere, Category=AmmoPickUp)
	EWeaponTypes WeaponType;

	UPROPERTY(EditAnywhere, Category=AmmoPickUp)
	int32 AddAmmo = 60;
};
