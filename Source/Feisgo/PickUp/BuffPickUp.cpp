// Fill out your copyright notice in the Description page of Project Settings.


#include "BuffPickUp.h"
#include "NiagaraFunctionLibrary.h"
#include "Feisgo/Character/ShooterCharacter.h"
#include "Kismet/GameplayStatics.h"

ABuffPickUp::ABuffPickUp()
{
	bReplicates = true;
	PickUpEffectComp = CreateDefaultSubobject<UNiagaraComponent>(TEXT("NiagaraComponent"));
	PickUpEffectComp->SetIsReplicated(true);
	PickUpEffectComp->SetupAttachment(GetRootComponent());
	
}

void ABuffPickUp::OnPickSphereOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	Super::OnPickSphereOverlap(OverlappedComponent, OtherActor, OtherComp, OtherBodyIndex, bFromSweep, SweepResult);

	if (HasAuthority()) {
		AShooterCharacter* HealShooter = Cast<AShooterCharacter>(OtherActor);
		if (HealShooter) {
			auto BuffComp = HealShooter->GetBuffComponent();
			if (BuffComp) {
				switch (BuffType) {
					case EPickUpTypes::EPUT_Health: {
						BuffComp->HealthBuff(IncreaseAmount, DurationTime);
					}break;
					case EPickUpTypes::EPUT_Jump:
					{
						BuffComp->JumpBuff(IncreaseAmount, DurationTime);
					}break;
					case EPickUpTypes::EPUT_Speed:
					{
						BuffComp->SpeedBuff(IncreaseAmount, DurationTime);	
					}break;
					case EPickUpTypes::EPUT_Shield:
					{
						BuffComp->ShieldBuff(IncreaseAmount);
					}
					default:
					{
						UE_LOG(LogTemp, Warning, TEXT("Unknown BuffType !"));
					}break;
				}
			}
		}
	}
	
	Destroy();
}

void ABuffPickUp::Destroyed()
{
	if (PickUpEffect) {
		UNiagaraFunctionLibrary::SpawnSystemAtLocation(this, PickUpEffect, GetActorLocation());	
	}
	
	Super::Destroyed();
}
