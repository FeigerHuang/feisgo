// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "PickUpBase.h"
#include "Feisgo/ShooterTypes/PickUpTypes.h"
#include "NiagaraComponent.h"
#include "BuffPickUp.generated.h"

/**
 * 
 */
UCLASS()
class FEISGO_API ABuffPickUp : public APickUpBase
{
	GENERATED_BODY()
public:
	ABuffPickUp();

protected:
	virtual void OnPickSphereOverlap(
	UPrimitiveComponent* OverlappedComponent,
	AActor* OtherActor,
	UPrimitiveComponent* OtherComp,
	int32 OtherBodyIndex,
	bool bFromSweep,
	const FHitResult& SweepResult);

	virtual void Destroyed() override;
private:

	UPROPERTY(VisibleAnywhere)
	UNiagaraComponent* PickUpEffectComp;

	UPROPERTY(EditAnywhere)
	UNiagaraSystem* PickUpEffect;

	UPROPERTY(EditAnywhere, Category=BuffProperty)
	EPickUpTypes BuffType;
	
	UPROPERTY(EditAnywhere, Category=BuffProperty)
	float IncreaseAmount;

	UPROPERTY(EditAnywhere, Category=BuffProperty)
	float DurationTime;
};

