// Fill out your copyright notice in the Description page of Project Settings.


#include "PickUpBase.h"

#include "Kismet/GameplayStatics.h"

// Sets default values
APickUpBase::APickUpBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	bReplicates = true;

	USceneComponent* RootComp = CreateDefaultSubobject<USceneComponent>(TEXT("RootComp"));
	SetRootComponent(RootComp);

	StaticMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("StaticMeshComp"));
	StaticMesh->SetWorldScale3D(FVector(3.5, 3.5, 3.5));
	StaticMesh->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	StaticMesh->SetupAttachment(RootComp);

	PickSphere = CreateDefaultSubobject<USphereComponent>(TEXT("PickSphere"));
	PickSphere->SetupAttachment(RootComp);
	PickSphere->SetSphereRadius(68.f);
	PickSphere->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	PickSphere->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Ignore);
	PickSphere->SetCollisionResponseToChannel(ECollisionChannel::ECC_Pawn, ECollisionResponse::ECR_Overlap);
	
}

// Called when the game starts or when spawned
void APickUpBase::BeginPlay()
{
	Super::BeginPlay();
	
	PickSphere->OnComponentBeginOverlap.AddDynamic(this, &ThisClass::OnPickSphereOverlap);

}

void APickUpBase::OnPickSphereOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{

	if (PickUpSound) {
		UGameplayStatics::PlaySoundAtLocation(this, PickUpSound, GetActorLocation());
	}
	
}

void APickUpBase::Destroyed()
{
	Super::Destroyed();
}

// Called every frame
void APickUpBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

