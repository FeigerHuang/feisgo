// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/SphereComponent.h"
#include "GameFramework/Actor.h"
#include "Sound/SoundCue.h"
#include "PickUpBase.generated.h"

UCLASS()
class FEISGO_API APickUpBase : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	APickUpBase();

	virtual void Tick(float DeltaTime) override;
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UFUNCTION()
	virtual void OnPickSphereOverlap(
		UPrimitiveComponent* OverlappedComponent,
		AActor* OtherActor,
		UPrimitiveComponent* OtherComp,
		int32 OtherBodyIndex,
		bool bFromSweep,
		const FHitResult& SweepResult);

	virtual void Destroyed() override;
	
	UPROPERTY(EditAnywhere, Category=PickUp)
	UStaticMeshComponent* StaticMesh;

	UPROPERTY(EditAnywhere, Category=PickUp)
	USphereComponent* PickSphere;
	
	UPROPERTY(EditAnywhere, Category=PickUp)
	USoundCue* PickUpSound;
};
