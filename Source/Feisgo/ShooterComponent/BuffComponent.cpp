// Fill out your copyright notice in the Description page of Project Settings.


#include "BuffComponent.h"

#include "Feisgo/Character/ShooterCharacter.h"
#include "GameFramework/CharacterMovementComponent.h"

// Sets default values for this component's properties
UBuffComponent::UBuffComponent()
{
	PrimaryComponentTick.bCanEverTick = true;
}


// Called when the game starts
void UBuffComponent::BeginPlay()
{
	Super::BeginPlay();
	
}

void UBuffComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	HealRampUp(DeltaTime);
}

void UBuffComponent::InitComponent(AShooterCharacter* ShooterCharacter)
{
	this->Shooter = ShooterCharacter;
	if (Shooter && Shooter->GetCharacterMovement()) {
		this->BaseWalkSpeed = Shooter->GetCharacterMovement()->MaxWalkSpeed;
		this->BaseJumpVelocity = Shooter->GetCharacterMovement()->JumpZVelocity;	
	}
}

void UBuffComponent::HealthBuff(float HealAmount, float HealingTime)
{
	if (Shooter == nullptr || !Shooter->HasAuthority()) return;
	HealRate = HealAmount / HealingTime;
	this->AmountToHeal = HealAmount;
	bHealing = true;
}


void UBuffComponent::HealRampUp(float DeltaTime)
{
	if (Shooter == nullptr || !bHealing || !Shooter->HasAuthority()) return;

	const float Recover = HealRate * DeltaTime;
	AmountToHeal -= Recover;
	Shooter->SetCurHealth(Shooter->GetCurHealth() + Recover);

	if (AmountToHeal <= 0.f) {
		bHealing = false;
	}
}

void UBuffComponent::ShieldBuff(float ShieldAmount)
{
	if (Shooter == nullptr || !Shooter->HasAuthority()) return;
	Shooter->SetCurShield(Shooter->GetCurShield() + ShieldAmount);
}


void UBuffComponent::SpeedBuff(float SpeedAdd, float DurationTime)
{
	if (Shooter == nullptr || !Shooter->HasAuthority()) return;
	Multi_SpeedBuff(SpeedAdd, DurationTime);
}

void UBuffComponent::Multi_SpeedBuff_Implementation(float SpeedAdd, float DurationTime)
{
	if (Shooter && Shooter->GetCharacterMovement()) {
		Shooter->GetCharacterMovement()->MaxWalkSpeed = BaseWalkSpeed + SpeedAdd;
		Shooter->GetWorldTimerManager().SetTimer(SpeedBuffTimerHandle,this, &ThisClass::OnSpeedUpEnd, DurationTime);
	}
}

void UBuffComponent::OnSpeedUpEnd()
{
	if (Shooter && Shooter->GetCharacterMovement()) {
		Shooter->GetCharacterMovement()->MaxWalkSpeed = BaseWalkSpeed;
	}
}


void UBuffComponent::JumpBuff(float JumpVelAdd, float DurationTime)
{
	if (Shooter == nullptr || !Shooter->HasAuthority()) return;
	Multi_JumpBuff(JumpVelAdd, DurationTime);
}

void UBuffComponent::Multi_JumpBuff(float JumpVelAdd, float DurationTime)
{
	if (Shooter && Shooter->GetCharacterMovement()) {
		Shooter->GetCharacterMovement()->JumpZVelocity += JumpVelAdd;
		Shooter->GetWorldTimerManager().SetTimer(JumpBuffTimerHandle,this, &ThisClass::OnJumpUpEnd, DurationTime);
	}
	
}

void UBuffComponent::OnJumpUpEnd()
{
	if (Shooter && Shooter->GetCharacterMovement()) {
		Shooter->GetCharacterMovement()->JumpZVelocity = BaseJumpVelocity;
	}
}
