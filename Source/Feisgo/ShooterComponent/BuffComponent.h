// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "BuffComponent.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class FEISGO_API UBuffComponent : public UActorComponent
{
	GENERATED_BODY()
public:	
	friend class AShooterCharacter;
	// Sets default values for this component's properties
	UBuffComponent();

	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	void InitComponent(AShooterCharacter* ShooterCharacter);

	void HealthBuff(float HealAmount, float HealingTime);

	void ShieldBuff(float ShieldAmount);
	
	void SpeedBuff(float SpeedAdd, float DurationTime);

	UFUNCTION(NetMulticast, Reliable)
	void Multi_SpeedBuff(float SpeedAdd, float DurationTime);

	void OnSpeedUpEnd();

	void JumpBuff(float JumpVelAdd, float DurationTime);

	void Multi_JumpBuff(float JumpVelAdd, float DurationTime);

	void OnJumpUpEnd();
protected:
	// Called when the game starts
	virtual void BeginPlay() override;

	/**
	 * Health buff
	 */
	void HealRampUp(float DeltaTime);

	AShooterCharacter* Shooter;

	UPROPERTY(VisibleAnywhere, Category=HealthBuff)
	bool bHealing = false;

	UPROPERTY(VisibleAnywhere, Category=HealthBuff)
	float HealRate = 0.f;

	UPROPERTY(VisibleAnywhere, Category=HealthBuff)
	float AmountToHeal = 0.f;

	/**
	 * Speed buff
	 */
	UPROPERTY(VisibleAnywhere, Category=SpeedBuff)
	float BaseWalkSpeed = 0.f;

	/**
	 * Jump buff
	 */
	UPROPERTY(VisibleAnywhere, Category=JumpBuff)
	float BaseJumpVelocity = 0.f;

private:
	FTimerHandle JumpBuffTimerHandle;
	FTimerHandle SpeedBuffTimerHandle;
};
