// Fill out your copyright notice in the Description page of Project Settings.


#include "CombatComponent.h"

#include "DrawDebugHelpers.h"
#include "Engine/SkeletalMeshSocket.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Kismet/GameplayStatics.h"
#include "Net/UnrealNetwork.h"

UCombatComponent::UCombatComponent()
	: bAimmed(false),
	BaseWalkSpeed(800),
	AimWalkSpeed(380)
{
	PrimaryComponentTick.bCanEverTick = true;
}

void UCombatComponent::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
	DOREPLIFETIME(ThisClass, bAimmed);
	DOREPLIFETIME(ThisClass, CurrentWeapon);
	DOREPLIFETIME(ThisClass, KnapsackWeapon);
	DOREPLIFETIME(ThisClass, CombatState);
	DOREPLIFETIME(ThisClass, GrenadeAmount);
	DOREPLIFETIME_CONDITION(ThisClass, CarriedAmmo, COND_OwnerOnly);
}

int32 UCombatComponent::AmmoReloadAmount()
{
	if (CurrentWeapon == nullptr) return 0;
	int32 RoomToAdd = CurrentWeapon->GetClipCapacity() - CurrentWeapon->GetClipAmmo();
	int32 LastAmmo = FMath::Min(RoomToAdd, CarriedAmmo);
	return LastAmmo;
}

void UCombatComponent::UpdateAmmoValue()
{
	if (Shooter == nullptr || CurrentWeapon == nullptr) return;
	const int32 AmmoToAdd = AmmoReloadAmount();
	if (CarriedAmmoMap.Contains(CurrentWeapon->GetWeaponType())) {
		CarriedAmmoMap[CurrentWeapon->GetWeaponType()] -= AmmoToAdd;
		CarriedAmmo = CarriedAmmoMap[CurrentWeapon->GetWeaponType()];
	}
	CurrentWeapon->IncreaseAmmo(AmmoToAdd);
}

void UCombatComponent::ShotgunAddAmmo()
{
	if (Shooter == nullptr || CurrentWeapon == nullptr) return;
	if (CarriedAmmoMap.Contains(EWeaponTypes::EWT_Shotgun)) {
		if (CurrentWeapon->GetWeaponType() != EWeaponTypes::EWT_Shotgun || CarriedAmmoMap[EWeaponTypes::EWT_Shotgun] == 0) {
			return;
		}

		CarriedAmmoMap[CurrentWeapon->GetWeaponType()] -= 1;
		CarriedAmmo = CarriedAmmoMap[CurrentWeapon->GetWeaponType()];
	}
	CurrentWeapon->IncreaseAmmo(1);

	OnRep_CarriedAmmo();
}

void UCombatComponent::FinishReloading()
{
	if (Shooter && Shooter->HasAuthority()) {
		CombatState = ECombatState::ECS_Unoccupied;
		
		UpdateAmmoValue();
		OnRep_CarriedAmmo();	
	}
}

void UCombatComponent::ThrowGrenade()
{
	if (CombatState != ECombatState::ECS_Unoccupied || GrenadeAmount <= 0) return;
	if (CurrentWeapon == nullptr) return;
	
	Server_SetCombatState(ECombatState::ECS_ThrowGrenade);
}

void UCombatComponent::Server_SetCombatState_Implementation(ECombatState NewState)
{
	CombatState = NewState;
	OnRep_CombatState();
}

void UCombatComponent::ThrowGrenadeHandle()
{
	if (Shooter) {
		Shooter->SetAttachedGrenadeVisible(true);
		if (CurrentWeapon) {
			CurrentWeapon->AttachToOwnerSocket("LeftHandSocket");
		}
		Shooter->PlayThrowGrenadeMontage();
	}
}

void UCombatComponent::FinishThrowGrenade()
{
	CombatState = ECombatState::ECS_Unoccupied;
	OnRep_CombatState();
}

void UCombatComponent::LaunchGrenade()
{
	if (Shooter) {
		Shooter->SetAttachedGrenadeVisible(false);
		if (Shooter->IsLocallyControlled()) {
			Server_LaunchGrenade(BulletHitResult.ImpactPoint);
		}
	}
}

void UCombatComponent::Server_LaunchGrenade_Implementation(const FVector& TargetLoc)
{
	if (Shooter->HasAuthority() && GrenadeClass) {
		FActorSpawnParameters SpawnParameters;
		SpawnParameters.Owner      = Shooter;
		SpawnParameters.Instigator = Shooter;
		FVector SpawnLoc           = Shooter->GetAttachedGrenadeLoc();
		FVector ToHitTarget        = TargetLoc - SpawnLoc;

		GetWorld()->SpawnActor<AGrenadeProjectile>(GrenadeClass, SpawnLoc, ToHitTarget.Rotation(), SpawnParameters);

		GrenadeAmount = FMath::Clamp(GrenadeAmount - 1, 0, INT32_MAX);
		OnRep_GrenadeAmount();
	}
}

void UCombatComponent::OnRep_GrenadeAmount()
{
	if (Shooter == nullptr || !Shooter->IsLocallyControlled()) 	return;
	ShooterController = (ShooterController == nullptr ? Cast<AShooterController>(Shooter->Controller) : ShooterController);
	if (ShooterController) {
		ShooterController->SetHUDGrenadeAmount(GrenadeAmount);
	}
}

void UCombatComponent::SprintButtonPress()
{
	Server_SetCombatState(ECombatState::ECS_Sprint);
}

void UCombatComponent::SprintButtonRelease()
{
	if (CombatState == ECombatState::ECS_Sprint) {
		Server_SetCombatState(ECombatState::ECS_Unoccupied);
	}
}

void UCombatComponent::SprintHandle(bool bSprint)
{
	if (Shooter == nullptr || Shooter->GetCharacterMovement() == nullptr) return;
	if (bSprint) {
		Shooter->GetCharacterMovement()->MaxWalkSpeed += SprintAddSpeed;
	} else {
		Shooter->GetCharacterMovement()->MaxWalkSpeed = BaseWalkSpeed;
	}
}

void UCombatComponent::BeginPlay()
{
	Super::BeginPlay();
	
	OnRep_GrenadeAmount();
}

void UCombatComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	if (Shooter && Shooter->IsLocallyControlled()) {

		SetWeaponCrosshairs(DeltaTime);
		
		OnAimInterpFOV(DeltaTime);
	}
}


void UCombatComponent::AimButtonPressed()
{
	SetAimmed(true);
	OnRep_Aimmed();	
	// if (Shooter && CurrentWeapon && CurrentWeapon->GetWeaponType() == EWeaponTypes::EWT_SniperRife) {
	// 	Shooter->SniperRifleZooming(true);	
	// }
}

void UCombatComponent::AimButtonReleased()
{
	SetAimmed(false);
	OnRep_Aimmed();	
	//
	// if (Shooter && CurrentWeapon && CurrentWeapon->GetWeaponType() == EWeaponTypes::EWT_SniperRife) {
	// 	Shooter->SniperRifleZooming(false);
	// }
}


void UCombatComponent::FireButtonPressed()
{
	bFireButtonPressed = true;

	if (CurrentWeapon == nullptr ) return ;
	
	Fire();
}

void UCombatComponent::FireButtonReleased()
{
	bFireButtonPressed = false;
	
	if (CurrentWeapon == nullptr) return;
	
	CurrentWeapon->OnFireEnd();
	
}

bool UCombatComponent::Fire() {

	if (CheckFireCondition()) {
		bCanFire = false;
		// Button Presses Happen in the localController;
		FHitResult HitResult;
		TraceUnderCrosshairs(HitResult);
		Server_FireBegin(HitResult.ImpactPoint);

		CurrentWeapon->OnFireBegin();

		StartFireTimer();

		return true;
	}
	return false;
	
}

void UCombatComponent::Server_FireBegin_Implementation(const FVector_NetQuantize& HitLocation)
{
	if (CurrentWeapon) {
		CombatState = ECombatState::ECS_Firing;
		CurrentWeapon->SpendRound();
		Multicast_FireBegin(HitLocation);
	}
}

void UCombatComponent::Multicast_FireBegin_Implementation(const FVector_NetQuantize& HitLocation)
{
	if (Shooter && CurrentWeapon && FireMontage) {
		UE_LOG(LogTemp, Warning, TEXT("Fire ButtonPressed"))
		FName StartSectionName = bAimmed ? FName("AimFire") : FName("HipFire");
		Shooter->PlayAnimMontage(FireMontage, 1.f, StartSectionName);
		CurrentWeapon->WeaponFire(HitLocation);
	}
}

bool UCombatComponent::CheckFireCondition() const
{
	if (CurrentWeapon == nullptr) return false;
	return !CurrentWeapon->ClipEmpty() && bCanFire;
}

void UCombatComponent::StartFireTimer()
{
	if (Shooter && CurrentWeapon) {
		Shooter->GetWorldTimerManager().SetTimer(FireTimerHandle, this, &ThisClass::OnFireTimerEnd, CurrentWeapon->FireDelay);
	}	
}

void UCombatComponent::OnFireTimerEnd()
{
	bCanFire = true;
	Server_SetCombatState(ECombatState::ECS_Unoccupied);
	
	if (CurrentWeapon && CurrentWeapon->ClipEmpty() && CarriedAmmo > 0) {
		Server_Reload();
	}
	if (CurrentWeapon && CurrentWeapon->bAutomaticFire && bFireButtonPressed) {
		Fire();
	}
}

void UCombatComponent::OnRep_CarriedAmmo()
{
	if (Shooter == nullptr || Shooter->Controller == nullptr) return;
	// Update Local Carried Ammo Map
	if (Shooter->IsLocallyControlled() && CurrentWeapon) {
		if (CarriedAmmoMap.Contains(CurrentWeapon->GetWeaponType())) {
			CarriedAmmoMap[CurrentWeapon->GetWeaponType()] = CarriedAmmo;
		} else {
			CarriedAmmoMap[CurrentWeapon->GetWeaponType()] = 0;
		}
	}
	
	ShooterController = (ShooterController == nullptr ? Cast<AShooterController>(Shooter->Controller) : ShooterController);
	if (ShooterController) {
		ShooterController->SetHUDCarriedAmmo(CarriedAmmo);	
	}
}

void UCombatComponent::Client_UpdateAmmoInfo_Implementation()
{
	OnRep_CarriedAmmo();
}


void UCombatComponent::SetWeaponCrosshairs(float DeltaTime) 
{
	if (Shooter == nullptr || Shooter->Controller == nullptr) return;
	ShooterController = (ShooterController == nullptr ? Cast<AShooterController>(Shooter->Controller) : ShooterController);
	
	if (ShooterController) {
		HUD = (HUD == nullptr ? Cast<AShooterHUD>(ShooterController->GetHUD()) : HUD);
		if (HUD && CurrentWeapon) {
			FHUDPacket LocalHUDPacket = CurrentWeapon->HUDPacket;

			// Calculate the Spread According to Condition
			LocalHUDPacket.CrosshairSpread = CalculateCrosshairsSpread(DeltaTime);
			// // do Trace
			TraceUnderCrosshairs(BulletHitResult);
			AActor* HitActor = BulletHitResult.GetActor();
			// if Tracing the Right Object, change Crosshairs color;
			if (HitActor && HitActor->Implements<UCrosshairReactInterface>()) {
				LocalHUDPacket.CrosshairColor = FLinearColor::Red;
			}
			AShooterCharacter* ShooterCharacter = Cast<AShooterCharacter>(HitActor);
			if (ShooterCharacter) {
				ShooterCharacter->ShowDisplayName();
			}
			HUD->SetHUDPacket(LocalHUDPacket);
		}
		else if (HUD) {
			HUD->SetHUDPacket(FHUDPacket());
		}
		
	}
}

float UCombatComponent::CalculateCrosshairsSpread(float DeltaTime)
{
	float ActualSpread = 0;
	float Speed = Shooter->GetVelocity().Size();
	// When Shooter moving , map walk speed to spread [0, 500] -> [0, 1]
	FVector2D SpeedRange(0, 500.f);
	FVector2D SpreadRange(0, 2.f);
	CrosshairVelocityFactor = FMath::GetMappedRangeValueClamped(SpeedRange, SpreadRange, Speed);

	if (Shooter->GetCharacterMovement()->IsFalling()) {
		CrosshairInAirFactor = FMath::FInterpTo(CrosshairInAirFactor, 3.f, DeltaTime, 3.f);
	} else {
		CrosshairInAirFactor = FMath::FInterpTo(CrosshairInAirFactor, 0.f, DeltaTime, 3.f);
	}

	if (Shooter->IsAimming()) {
		CrosshairAimmingFactor = FMath::FInterpTo(CrosshairAimmingFactor, -0.5f, DeltaTime, 3.f);
	} else {
		CrosshairAimmingFactor = FMath::FInterpTo(CrosshairAimmingFactor, 1.f, DeltaTime, 3.f);
	}
	
	const float Recoil = CurrentWeapon->GetRocil() * 5.f;
	CrosshairShootFactor = FMath::FInterpTo(CrosshairShootFactor, Recoil, DeltaTime, 20.f);
	
	ActualSpread = CrosshairVelocityFactor + CrosshairInAirFactor + CrosshairAimmingFactor + CrosshairShootFactor;


	return ActualSpread;
	
}

void UCombatComponent::Reload()
{
	if (CurrentWeapon == nullptr || CurrentWeapon->ClipFull()) return;
	if (CarriedAmmo > 0 && CombatState == ECombatState::ECS_Unoccupied) {
		Server_Reload();
	}
}

void UCombatComponent::Server_Reload_Implementation()
{
	if (Shooter == nullptr) return;
	CombatState = ECombatState::ECS_Reloading;
	ReloadHandle();
}

void UCombatComponent::ReloadHandle()
{
	Shooter->PlayReloadMontage();
}

void UCombatComponent::OnRep_CombatState()
{
	switch (CombatState) {
		case ECombatState::ECS_Reloading:
		{
			ReloadHandle();
		}break;
		case ECombatState::ECS_ThrowGrenade:
		{
			ThrowGrenadeHandle();	
		}break;
		case ECombatState::ECS_Sprint:
		{
			SprintHandle(true);
		}break;
		default:
		{
			SprintHandle(false);
			if (CurrentWeapon) {
				CurrentWeapon->AttachToOwnerSocket("RightHandSocket");
			}
		}break;
	}	
}

void UCombatComponent::InitCombatComp(AShooterCharacter* NewShooter)
{
	this->Shooter = NewShooter;
	if (NewShooter != nullptr) {
		this->BaseWalkSpeed = Shooter->GetCharacterMovement()->MaxWalkSpeed;
		this->DefaultFOV = Shooter->GetCameraFOV();
		this->CurrentAimFOV = DefaultFOV;
		this->ShooterController = Cast<AShooterController>(Shooter->Controller);
	}
}

void UCombatComponent::ChangeShooterSpeed()
{
	if (Shooter == nullptr) return;

	if (bAimmed == true) {
		Shooter->GetCharacterMovement()->MaxWalkSpeed = AimWalkSpeed;
	} else {
		Shooter->GetCharacterMovement()->MaxWalkSpeed = BaseWalkSpeed;
	}
}

void UCombatComponent::OnRep_Aimmed()
{
	UE_LOG(LogTemp, Error, TEXT("Aimming=%d"), bAimmed);
	if (Shooter == nullptr || !Shooter->IsLocallyControlled()) return;
	if (bAimmed && GetCurrentWeaponTypes() == EWeaponTypes::EWT_SniperRife) {
		Shooter->SniperRifleZooming(true);
	} else if (GetCurrentWeaponTypes() == EWeaponTypes::EWT_SniperRife) {
		Shooter->SniperRifleZooming(false);
	}
}

void UCombatComponent::TraceUnderCrosshairs(FHitResult& HitResult)
{
	FVector2D ViewPortSize;
	FVector2D ViewCenterLoc;
	FVector   AimLocation;
	FVector   AimDirection;
	if (GEngine && GEngine->GameViewport) {
		GEngine->GameViewport->GetViewportSize(ViewPortSize);
	}

	ViewCenterLoc.X = ViewPortSize.X / 2.f;
	ViewCenterLoc.Y = ViewPortSize.Y / 2.f;

	APlayerController* PlayerController = UGameplayStatics::GetPlayerController(this, 0);
	bool bSuccess = UGameplayStatics::DeprojectScreenToWorld(PlayerController, ViewCenterLoc, AimLocation, AimDirection);
	
	if (bSuccess) {
		FVector EndPosition = AimLocation + AimDirection * TRACE_LEN;
		// Aim location need add a coup of distance
		FCollisionQueryParams QueryParams;
		if (Shooter && CurrentWeapon) {
			QueryParams.AddIgnoredActor(Shooter);

			FVector MuzzleLocation = CurrentWeapon->GetMuzzleLocation();
			AimLocation += AimDirection * ((MuzzleLocation - AimLocation).Size() + 100.f);
		}
		QueryParams.bTraceComplex = false;
			
		GetWorld()->LineTraceSingleByChannel(
			HitResult,
			AimLocation,
			EndPosition,
			ECollisionChannel::ECC_Visibility,
			QueryParams);

		if (!HitResult.bBlockingHit) {
			HitResult.ImpactPoint = EndPosition;
		}
	}
}

void UCombatComponent::OnRep_EquippedWeapon()
{
	if (CurrentWeapon) {
		this->WeaponAimFOV = CurrentWeapon->GetZoomFOV();
		this->WeaponZoomSpeed = CurrentWeapon->GetZoomSpeed();
	}
}


void UCombatComponent::OnAimInterpFOV(float DeltaTime)
{
	//if (CurrentWeapon == nullptr) return;
	
	if (bAimmed) {
		CurrentAimFOV = FMath::FInterpTo(CurrentAimFOV, WeaponAimFOV, DeltaTime, WeaponZoomSpeed);
		Shooter->SetCameraFOV(CurrentAimFOV);
	} else {
		CurrentAimFOV = FMath::FInterpTo(CurrentAimFOV, DefaultFOV, DeltaTime, DefaultZoomSpeed);
		Shooter->SetCameraFOV(CurrentAimFOV);
	}
}


void UCombatComponent::SetAimmed(bool isAimming)
{
	bAimmed = isAimming;
	ChangeShooterSpeed();
	// told server change current bAimmed state;
	Server_SetAimmed(isAimming);
}


void UCombatComponent::Server_SetAimmed_Implementation(bool IsAimming)
{
	bAimmed = IsAimming;
	ChangeShooterSpeed();
}


void UCombatComponent::Server_EquipWeapon_Implementation(AWeapon* WeaponToEquip)
{
	if (Shooter && WeaponToEquip) {
		// 当背包武器存在,先替换当前武器 , 或当前武器不存在, 直接装备;
		if (KnapsackWeapon != nullptr || CurrentWeapon == nullptr) {
			DropCurrentWeapon();
			// Owner is Rep var;
			WeaponToEquip->SetOwner(Shooter);

			WeaponToEquip->SetWeaponState(EWeaponState::EWS_Equiped);

			// Set Player Equip state
			Shooter->SetIsEquipped(true);
			CurrentWeapon = WeaponToEquip;

			UpdateAmmoInfo();		
		} else { // 否则就是 背包武器为空的情况;
			WeaponToEquip->SetOwner(Shooter);
			WeaponToEquip->SetWeaponState(EWeaponState::EWS_Knapsack);
			KnapsackWeapon = WeaponToEquip;
		}
	}
}

void UCombatComponent::Server_TogglePackWeapon_Implementation()
{
	if (Shooter == nullptr) return;
	Swap(CurrentWeapon, KnapsackWeapon);
	SetAimmed(false);
	
	if (CurrentWeapon) {
		CurrentWeapon->SetWeaponState(EWeaponState::EWS_Equiped);
		Shooter->SetIsEquipped(true);
		UpdateAmmoInfo();
	} else {
		Shooter->SetIsEquipped(false);
	}

	if (KnapsackWeapon) {
		KnapsackWeapon->SetWeaponState(EWeaponState::EWS_Knapsack);
	}
}

void UCombatComponent::UpdateAmmoInfo()
{
	if (CurrentWeapon == nullptr) return;
	if (CarriedAmmoMap.Contains(CurrentWeapon->GetWeaponType())) {
		CarriedAmmo = CarriedAmmoMap[CurrentWeapon->GetWeaponType()];
		
		// Because this happen in the server, we need call this again;
		OnRep_CarriedAmmo();

		if (CarriedAmmo == 0) {
			// 因为当客户端的子弹和复制后的数量一样时,不会调用OnRep, 我们需要手动调用;
			Client_UpdateAmmoInfo();	
		}
	} 
}

void UCombatComponent::PickUpAmmo(EWeaponTypes WeaponType, int32 AddAmmo)
{
	if (WeaponType != EWeaponTypes::EWT_MAX && AddAmmo > 0) {
		CarriedAmmoMap[WeaponType] += AddAmmo;
		UpdateAmmoInfo();
	}	
}

EWeaponTypes UCombatComponent::GetCurrentWeaponTypes() const
{
	if (CurrentWeapon) {
		return CurrentWeapon->GetWeaponType();	
	}
	return EWeaponTypes::EWT_MAX;
}

int32 UCombatComponent::GetClipAmmo() const
{
	if (CurrentWeapon) return CurrentWeapon->GetClipAmmo();
	return 0;
}

void UCombatComponent::Server_DropWeapon_Implementation()
{
	DropCurrentWeapon();
}

void UCombatComponent::DropCurrentWeapon()
{
	if (Shooter && CurrentWeapon) {

		FDetachmentTransformRules DetachRules(EDetachmentRule::KeepWorld, true);
		CurrentWeapon->DetachFromActor(DetachRules);
		
		CurrentWeapon->SetOwner(nullptr);
		CurrentWeapon->SetWeaponState(EWeaponState::EWS_Dropped);
		Shooter->SetIsEquipped(false);

		CurrentWeapon = nullptr;
		bAimmed = false;
	}
}

void UCombatComponent::DropKnapsackWeapon()
{
	if (Shooter && KnapsackWeapon) {
		FDetachmentTransformRules DetachRules(EDetachmentRule::KeepWorld, true);
        KnapsackWeapon->DetachFromActor(DetachRules);
		KnapsackWeapon->SetOwner(nullptr);
        KnapsackWeapon->SetWeaponState(EWeaponState::EWS_Dropped);

		KnapsackWeapon = nullptr;
	}	
}
