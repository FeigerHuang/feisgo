// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "Feisgo/Character/ShooterCharacter.h"
#include "Feisgo/Character/ShooterController.h"
#include "Feisgo/GamePlay/GrenadeProjectile.h"
#include "Feisgo/ShooterTypes/CombatTypes.h"
#include "Feisgo/UI/ShooterHUD.h"
#include "Feisgo/ShooterTypes/WeaponTypes.h"
#include "CombatComponent.generated.h"


DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnWeaponEquippedDelegate);

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class FEISGO_API UCombatComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	UCombatComponent();

	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;
	
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	UFUNCTION(Server, Reliable)	
	void Server_EquipWeapon(AWeapon* WeaponToEquip);

	/**
	 * Switch current weapon and KnapsackWeapon
	 */
	UFUNCTION(Server, Reliable)
	void Server_TogglePackWeapon();
	
	UFUNCTION(Server, Reliable)	
	void Server_DropWeapon();

	void DropCurrentWeapon();

	void DropKnapsackWeapon();
	
	UFUNCTION(BlueprintCallable)
	void InitCombatComp(AShooterCharacter* NewShooter); 

	UFUNCTION(BlueprintCallable)
	AShooterCharacter* GetShooter() const {return Shooter;}

	void AimButtonPressed();

	void AimButtonReleased();

	FORCEINLINE bool IsAiming() const {return bAimmed;}

	void FireButtonPressed();

	void FireButtonReleased();

	UFUNCTION(Server, Reliable)
	void Server_FireBegin(const FVector_NetQuantize& HitLocation);

	UFUNCTION(NetMulticast, Reliable)
	void Multicast_FireBegin(const FVector_NetQuantize& HitLocation);

	FOnWeaponEquippedDelegate OnWeaponEquippedDelegate;

	void SetWeaponCrosshairs(float DeltaTime);

	float CalculateCrosshairsSpread(float DeltaTime);

	void Reload();

	void ReloadHandle();
	
	UFUNCTION(Server, Reliable)
	void Server_Reload();

	int32 AmmoReloadAmount();

	void UpdateAmmoValue();

	UFUNCTION(BlueprintAuthorityOnly)
	void ShotgunAddAmmo();
	
	// just call in the Server
	UFUNCTION(BlueprintAuthorityOnly)
	void FinishReloading();
	
	/**
	 * Grenade Relative
	 */
	void ThrowGrenade();

	UFUNCTION(Server, Reliable)
	void Server_SetCombatState(ECombatState NewState);

	void ThrowGrenadeHandle();

	UFUNCTION(BlueprintAuthorityOnly)
	void FinishThrowGrenade();

	UFUNCTION(BlueprintCallable)
	void LaunchGrenade();

	UFUNCTION(Server, Reliable)
	void Server_LaunchGrenade(const FVector& TargetLoc);
	
	UPROPERTY(EditAnywhere)
	TSubclassOf<AGrenadeProjectile> GrenadeClass;

	UPROPERTY(EditAnywhere, ReplicatedUsing=OnRep_GrenadeAmount)
	int32 GrenadeAmount = 0;

	UFUNCTION()
	void OnRep_GrenadeAmount();

	void SprintButtonPress();

	void SprintButtonRelease();

	void SprintHandle(bool bSprint);
protected:
	virtual void BeginPlay() override;

	UFUNCTION(Server, Reliable)
	void Server_SetAimmed(bool IsAimming);

	void SetAimmed(bool isAimming);

	void ChangeShooterSpeed();

	UFUNCTION()
	void OnRep_Aimmed();
	
	void TraceUnderCrosshairs(FHitResult& HitResult);

	UFUNCTION()
	void OnRep_EquippedWeapon();
	
	UPROPERTY(VisibleAnywhere)
	FHitResult BulletHitResult;
private:
	AShooterCharacter* Shooter;
	AShooterController* ShooterController;

	UPROPERTY(ReplicatedUsing=OnRep_EquippedWeapon)
	AWeapon* CurrentWeapon;
	
	UPROPERTY(Replicated)
	AWeapon* KnapsackWeapon;

	UPROPERTY(ReplicatedUsing=OnRep_Aimmed)
	bool bAimmed = false;

	UPROPERTY(EditAnywhere)
	UAnimMontage* FireMontage;

	UPROPERTY(ReplicatedUsing=OnRep_Combatstate)
	ECombatState CombatState = ECombatState::ECS_Unoccupied;

	UFUNCTION()
	void OnRep_CombatState();
	/**
	 * HUD and Crosshairs
	 *
	 ***/
	AShooterHUD* HUD;
	
	FHUDPacket HUDPacket;
	
	float CrosshairVelocityFactor;
	float CrosshairInAirFactor;
	float CrosshairAimmingFactor;
	float CrosshairShootFactor;
	/**
	 *	Weapon Aim Zoom in
	 **/
	
	UPROPERTY(EditAnywhere, Category=CameraFOV)
	float DefaultFOV = 90.f;
	UPROPERTY(EditAnywhere, Category=CameraFOV)
	float DefaultZoomSpeed = 10.f;
	
	float WeaponAimFOV = 30.f;
	float WeaponZoomSpeed = 20.f;

	float CurrentAimFOV;
	void OnAimInterpFOV(float DeltaTime);

	/*
	 * Automatic Fire 
	 */
	bool Fire();

	bool bCanFire = true;

	bool CheckFireCondition() const;
	
	void StartFireTimer();

	void OnFireTimerEnd();
	
	FTimerHandle FireTimerHandle;

	bool bFireButtonPressed = false;
public:
	
	UPROPERTY(EditAnywhere)
	float BaseWalkSpeed;

	UPROPERTY(EditAnywhere)
	float AimWalkSpeed;

	UPROPERTY(EditAnywhere)
	float SprintAddSpeed;
	/**
	 * Ammo capacity;
	 */
	UPROPERTY(EditDefaultsOnly, Category = "Ammo")
	TMap<EWeaponTypes, int32> CarriedAmmoMap;

	UPROPERTY(ReplicatedUsing=OnRep_CarriedAmmo)
	int32 CarriedAmmo;

	UFUNCTION()
	void OnRep_CarriedAmmo();	

	UFUNCTION(Client, Reliable)
	void Client_UpdateAmmoInfo();
	
	void UpdateAmmoInfo();

	void PickUpAmmo(EWeaponTypes WeaponType, int32 AddAmmo);
		
	FORCEINLINE AWeapon* GetEquippedWeapon() const {return CurrentWeapon;}
	FORCEINLINE AWeapon* GetKnapsackWeapon() const {return KnapsackWeapon;}
	EWeaponTypes GetCurrentWeaponTypes() const;
	FORCEINLINE ECombatState GetCombatState() const {return CombatState;}
	FORCEINLINE int32 GetCarriedAmmo() const {return CarriedAmmo;}
	int32 GetClipAmmo() const;
};


