﻿#pragma once

UENUM(BlueprintType)
enum class ECombatState : uint8
{
	ECS_Unoccupied UMETA(DisplayName="Unoccupied"),
	ECS_Reloading UMETA(DisplayName="Reloading"),
	ECS_Firing UMETA(DisplayName="Firing"),
	ECS_ThrowGrenade UMETA(DisplayName="ThrowGrenade"),
	ECS_Sprint UMETA(DisplayName="Sprint"),
	ECS_MAX UMETA(DisplayName="DefaultMax")
};