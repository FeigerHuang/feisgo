﻿#pragma once

UENUM(BlueprintType)
enum class EPickUpTypes : uint8
{
	EPUT_Health UMETA(DisplayName="PickUp_Health"),
	EPUT_Speed UMETA(DisplayName="PickUp_Speed"),
	EPUT_Jump UMETA(DisplayName="PickUp_Jump"),
	EPUT_Shield UMETA(DisplayName="PickUp_Shield"),
	EPUT_Max UMETA(DisplayName="DefaultMax")
};
