﻿#pragma once

UENUM(BlueprintType)
enum class ETurnInPlace : uint8
{
	ETIP_NoTurnning UMETA(DisplayName="Not Turnning"),
	ETIP_TurnLeft UMETA(DisplayName="Turn Left"),
	ETIP_TurnRight UMETA(DisplayName="Turn Right"),
	ETIP_DefaulMax UMETA(DisplayName="Defualt_Max")
};