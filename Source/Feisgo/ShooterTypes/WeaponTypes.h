﻿#pragma once

#define TRACE_LEN 80000.f

#define CUSTOM_DEPTH_PURPLE 250
#define CUSTOM_DEPTH_BLUE 251
#define CUSTOM_DEPTH_TAN 252

UENUM(BlueprintType)
enum class EWeaponTypes : uint8
{
	EWT_AssaultRife UMETA(DisplayName="Assault Rifle"),
	EWT_RocketLauncher UMETA(DisplayName="Rocket Launcher"),
	EWT_Pistol UMETA(DisplayName="Pistol"),
	EWT_Shotgun UMETA(DisplayName="Shotgun"),
	EWT_SniperRife UMETA(DisplayName="SniperRife"),
	EWT_GrenadeLauncher UMETA(DisplayName="Grenade Launcher"),
	EWT_MAX UMETA(DisplayName="DefualtMax")
};