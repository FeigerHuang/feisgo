// Fill out your copyright notice in the Description page of Project Settings.


#include "Announcement.h"

void UAnnouncement::SetWarmupTime(FText InText)
{
	if (WarmupTime) {
		WarmupTime->SetText(InText);
	}
}

void UAnnouncement::SetAnnouncementText(FText InText)
{
	if (AnnouncementText) {
		AnnouncementText->SetText(InText);
	}
}

void UAnnouncement::SetInfoText(FText InText)
{
	if (InfoText) {
		InfoText->SetText(InText);
	}
}
