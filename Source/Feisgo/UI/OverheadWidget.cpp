// Fill out your copyright notice in the Description page of Project Settings.


#include "OverheadWidget.h"

#include "GameFramework/Character.h"
#include "GameFramework/PlayerState.h"

void UOverheadWidget::SetDisplayText(FString TextToDisplay)
{
	if (DisplayText) {
		DisplayText->SetText(FText::FromString(TextToDisplay));
	}
}

void UOverheadWidget::ShowPlayerName(ACharacter* Character)
{
	if (Character) {
		APlayerState* PlayerState = Character->GetPlayerState();
		if (PlayerState) {
			SetDisplayText(PlayerState->GetPlayerName());
		}
	}
}

void UOverheadWidget::OnLevelRemovedFromWorld(ULevel* InLevel, UWorld* InWorld)
{
	Super::OnLevelRemovedFromWorld(InLevel, InWorld);
}
