// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "Components/TextBlock.h"
#include "OverheadWidget.generated.h"

/**
 * 
 */
UCLASS()
class FEISGO_API UOverheadWidget : public UUserWidget
{
	GENERATED_BODY()
public:
	void SetDisplayText(FString TextToDisplay);

	UFUNCTION(BlueprintCallable)
	void ShowPlayerName(ACharacter* Character);
	
	UPROPERTY(meta=(BindWight))
	UTextBlock* DisplayText;
protected:
	virtual void OnLevelRemovedFromWorld(ULevel* InLevel, UWorld* InWorld) override;
};
