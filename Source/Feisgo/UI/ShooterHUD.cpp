// Fill out your copyright notice in the Description page of Project Settings.


#include "ShooterHUD.h"

#include "Feisgo/Character/ShooterController.h"

void AShooterHUD::BeginPlay()
{
	Super::BeginPlay();

	AddShooterOverlay();
	SetShooterOverlayVisible(false);
	AddAnnouncement();
}

void AShooterHUD::SetHealthText(float Health)
{
	int32 CeilHealth = FMath::CeilToInt(Health);
	FString HealthString = FString::Printf(TEXT("HP: %d"), CeilHealth);
	if (ShooterOverlay) {
		ShooterOverlay->SetHealthText(FText::FromString(HealthString));
	}
}

void AShooterHUD::SetHealthTextVisible(bool bVisible)
{
	if (ShooterOverlay == nullptr) return;
	if (bVisible) {
		ShooterOverlay->SetHealthTextVisible(ESlateVisibility::Visible);
	} else {
		ShooterOverlay->SetHealthTextVisible(ESlateVisibility::Hidden);
	}
}

void AShooterHUD::SetShieldText(int32 Shield)
{
	FString ShieldString = FString::Printf(TEXT("%02d"), Shield);
	if (ShooterOverlay) {
		ShooterOverlay->SetShieldText(FText::FromString(ShieldString));
	}
}

void AShooterHUD::UpdateKillAmount(int32 KillAmount, float FadeTime)
{
	FString KillString = FString::Printf(TEXT("%d Kill"), KillAmount);
	if (ShooterOverlay) {
		ShooterOverlay->SetKillAmount(FText::FromString(KillString));
		ShooterOverlay->FadeKillAmount(FadeTime);
	}
}

void AShooterHUD::UpdateClipAmmoAmount(int32 ClipAmmo)
{
	FString ClipAmmoString = FString::Printf(TEXT("%d/"), ClipAmmo);
	if (ShooterOverlay) {
		ShooterOverlay->SetClipAmmoAmount(FText::FromString(ClipAmmoString));
	}
}

void AShooterHUD::UpdateCarriedAmmoAmount(int32 CarriedAmmo)
{
	FString CarriedAmmoString = FString::Printf(TEXT("%d"), CarriedAmmo);
	if (ShooterOverlay) {
		ShooterOverlay->SetCarriedAmmoAmount(FText::FromString(CarriedAmmoString));
	}
}

void AShooterHUD::UpdateWeaponType(EWeaponTypes NewWeaponType)
{
	if (ShooterOverlay) {
		ShooterOverlay->SetCurWeaponType(NewWeaponType);
	}
}


void AShooterHUD::SetAmmoHintVisible(bool bVisible)
{
	if (!ShooterOverlay) return;
	if (bVisible) {
		ShooterOverlay->SetAmmoHintVisible(ESlateVisibility::Visible);
	} else {
		ShooterOverlay->SetAmmoHintVisible(ESlateVisibility::Hidden);
	}
}

void AShooterHUD::UpdateRemainTime(int32 RemainSeconds)
{
	if (!ShooterOverlay) return;
	RemainSeconds = RemainSeconds < 0 ? 0 : RemainSeconds;

	int32 RemainMinutes = RemainSeconds / 60;
	RemainSeconds %= 60;
	FString TimeString  = FString::Printf(TEXT("%02d:%02d"), RemainMinutes, RemainSeconds);
	ShooterOverlay->SetTimeRemain(FText::FromString(TimeString));
}

void AShooterHUD::SetGrenadeAmount(int32 Amount)
{
	if (!ShooterOverlay) return;
	FString AmountString = FString::Printf(TEXT("%d"), Amount);
	ShooterOverlay->SetGrenadeAmount(FText::FromString(AmountString));
}

void AShooterHUD::AddShooterOverlay()
{
	AShooterController* ShooterController = Cast<AShooterController>(GetOwningPlayerController());
	if (ShooterController && ShooterOverlayClass) {
		ShooterOverlay = CreateWidget<UShooterOverlay>(ShooterController, ShooterOverlayClass);
		if (ShooterOverlay) {
			ShooterOverlay->AddToViewport();
		}
	}
}

void AShooterHUD::AddAnnouncement()
{
	AShooterController* ShooterController = Cast<AShooterController>(GetOwningPlayerController());
	if (ShooterController && AnnouncementClass) {
		Announcement = CreateWidget<UAnnouncement>(ShooterController, AnnouncementClass);
		Announcement->AddToViewport();
	}
}

void AShooterHUD::SetShooterOverlayVisible(bool bVisible)
{
	if (ShooterOverlay == nullptr) return;
	if (bVisible) {
		ShooterOverlay->SetVisibility(ESlateVisibility::Visible);
	} else {
		ShooterOverlay->SetVisibility(ESlateVisibility::Hidden);
	}
}

void AShooterHUD::SetAnnouncementVisible(bool bVisible)
{
	if (Announcement == nullptr) return;
	if (bVisible) {
		Announcement->SetVisibility(ESlateVisibility::Visible);
	} else {
		Announcement->SetVisibility(ESlateVisibility::Hidden);
	}
}

void AShooterHUD::UpdateWarmupTime(int32 RemainSeconds)
{
	if (Announcement == nullptr) return;
	
	RemainSeconds = RemainSeconds < 0 ? 0 : RemainSeconds;

	int32 RemainMinutes = RemainSeconds / 60;
	RemainSeconds %= 60;
	FString TimeString  = FString::Printf(TEXT("%02d:%02d"), RemainMinutes, RemainSeconds);
	Announcement->SetWarmupTime(FText::FromString(TimeString));
}

void AShooterHUD::SetAnnouncementText(FText InText)
{
	if (Announcement) {
		Announcement->SetAnnouncementText(InText);
	}
}

void AShooterHUD::SetInfoText(FText InText)
{
	if (Announcement) {
		Announcement->SetInfoText(InText);
	}
}

void AShooterHUD::DrawHUD()
{
	Super::DrawHUD();

	if (GEngine) {
		FVector2D ViewPortSize;
		GEngine->GameViewport->GetViewportSize(ViewPortSize);
		const FVector2D ViewPortCenter(ViewPortSize.X / 2.f, ViewPortSize.Y / 2.f);
		
		float SpreadScaled = CrosshairSpreadMax * HUDPacket.CrosshairSpread;
		if (HUDPacket.CrosshairsCenter) {
			DrawCrosshairs(HUDPacket.CrosshairsCenter, ViewPortCenter, FVector2D(0.f,0.f), HUDPacket.CrosshairColor);
		}
		if (HUDPacket.CrosshairsLeft) {
			DrawCrosshairs(HUDPacket.CrosshairsLeft, ViewPortCenter, FVector2D(-SpreadScaled, 0.f), HUDPacket.CrosshairColor);
		}
		if (HUDPacket.CrosshairsRight) {
			DrawCrosshairs(HUDPacket.CrosshairsRight, ViewPortCenter, FVector2D(SpreadScaled, 0.f), HUDPacket.CrosshairColor);
		}
		if (HUDPacket.CrosshairsTop) {
			DrawCrosshairs(HUDPacket.CrosshairsTop, ViewPortCenter, FVector2D(0.f,-SpreadScaled), HUDPacket.CrosshairColor);
		}
		if (HUDPacket.CrosshairsBottom) {
			DrawCrosshairs(HUDPacket.CrosshairsBottom, ViewPortCenter, FVector2D(0, SpreadScaled), HUDPacket.CrosshairColor);
		}
	}
}


void AShooterHUD::DrawCrosshairs(UTexture2D* TexTure, FVector2D ViewPortCenter, FVector2D Spread, FLinearColor DrawColor)
{
	const float TextureWidth = TexTure->GetSizeX();
	const float TextureHeight = TexTure->GetSizeY();

	const FVector2D DrawPosition(ViewPortCenter.X - TextureWidth / 2.f + Spread.X,
		ViewPortCenter.Y - TextureHeight / 2.f + Spread.Y);

	DrawTexture(
		TexTure,
		DrawPosition.X,
		DrawPosition.Y,
		TextureWidth,
		TextureHeight,
		0.f,
		0.f,
		1.f,
		1.f,
		DrawColor
		);
}
