// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Announcement.h"
#include "ShooterOverlay.h"
#include "GameFramework/HUD.h"
#include "ShooterHUD.generated.h"


/**
 * 
 */
USTRUCT(BlueprintType)
struct FHUDPacket
{
	GENERATED_BODY()
public:
	FHUDPacket() = default;
	FHUDPacket(UTexture2D* TextCenter, UTexture2D* TextLeft, UTexture2D* TextRight, UTexture2D* TextTop, UTexture2D* TextBottom)
		: CrosshairsCenter(TextCenter),
		CrosshairsLeft(TextLeft),
		CrosshairsRight(TextRight),
		CrosshairsTop(TextTop),
		CrosshairsBottom(TextBottom),
		CrosshairSpread(0.f),
		CrosshairColor(FLinearColor::White)
	{
		
	}
	FHUDPacket(const FHUDPacket& Other)
		: CrosshairsCenter(Other.CrosshairsCenter),
		CrosshairsLeft(Other.CrosshairsLeft),
		CrosshairsRight(Other.CrosshairsRight),
		CrosshairsTop(Other.CrosshairsTop),
		CrosshairsBottom(Other.CrosshairsBottom),
		CrosshairSpread(Other.CrosshairSpread),
		CrosshairColor(Other.CrosshairColor)
	{
		
	}

	UPROPERTY(EditAnywhere)
	UTexture2D* CrosshairsCenter;
	UPROPERTY(EditAnywhere)
	UTexture2D* CrosshairsLeft;
	UPROPERTY(EditAnywhere)
	UTexture2D* CrosshairsRight;
	UPROPERTY(EditAnywhere)
	UTexture2D* CrosshairsTop;
	UPROPERTY(EditAnywhere)
	UTexture2D* CrosshairsBottom;
	UPROPERTY(EditAnywhere)
	float CrosshairSpread;
	UPROPERTY(EditAnywhere)
	FLinearColor CrosshairColor;
};


UCLASS()
class FEISGO_API AShooterHUD : public AHUD
{
	GENERATED_BODY()
public:
	/** The Main Draw loop for the hud.  Gets called before any messaging.  Should be subclassed 系统会受到消息后调用*/ 
	virtual void DrawHUD() override;

	FORCEINLINE void SetHUDPacket(const FHUDPacket& Packet) {HUDPacket = Packet;}

	void SetHealthText(float Health);
	
	void SetHealthTextVisible(bool bVisible);

	void SetShieldText(int32 Shield);
	
	void UpdateKillAmount(int32 KillAmount, float FadeTime);

	void UpdateClipAmmoAmount(int32 ClipAmmo);

	void UpdateCarriedAmmoAmount(int32 CarriedAmmo);

	void UpdateWeaponType(EWeaponTypes NewWeaponType);

	void SetAmmoHintVisible(bool bVisible);

	void UpdateRemainTime(int32 RemainSeconds);

	void SetGrenadeAmount(int32 Amount);
	
	void SetShooterOverlayVisible(bool bVisible);

	void SetAnnouncementVisible(bool bVisible);

	void UpdateWarmupTime(int32 RemainSeconds);

	void SetAnnouncementText(FText InText);

	void SetInfoText(FText InText);
protected:
	virtual void BeginPlay() override;

	void AddShooterOverlay();
	
	UPROPERTY(EditAnywhere)
	TSubclassOf<UShooterOverlay> ShooterOverlayClass;

	UPROPERTY()
	UShooterOverlay* ShooterOverlay;

	void AddAnnouncement();

	UPROPERTY(EditAnywhere)
	TSubclassOf<UAnnouncement> AnnouncementClass;

	UPROPERTY()
	UAnnouncement* Announcement;
private:
	FHUDPacket HUDPacket;

	UPROPERTY(EditAnywhere)
	float CrosshairSpreadMax = 15.f;
	
	void DrawCrosshairs(UTexture2D* TexTure, FVector2D ViewPortCenter, FVector2D Spread, FLinearColor DrawColor);
};
