// Fill out your copyright notice in the Description page of Project Settings.


#include "ShooterOverlay.h"

#include <basetyps.h>

void UShooterOverlay::SetHealthText(FText InText)
{
	if (HealthText) {
		HealthText->SetText(InText);
	}
}

void UShooterOverlay::SetHealthTextVisible(ESlateVisibility Visible)
{
	if (HealthText) {
		HealthText->SetVisibility(Visible);
	}
}

void UShooterOverlay::SetShieldText(FText InText)
{
	if (ShieldText) {
		ShieldText->SetText(InText);
	}
}

void UShooterOverlay::SetKillAmount(FText InText)
{
	if (KillAmount) {
		KillAmount->SetText(InText);
	}
}

void UShooterOverlay::FadeKillAmount(float Time)
{
	if (Time == 0.f) return;
	UWorld* World = GetWorld();
	auto HiddenFunc = [&]{
		if (KillAmount) {
			KillAmount->SetVisibility(ESlateVisibility::Hidden);
		}
	};
	
	if (World) {
		KillAmount->SetVisibility(ESlateVisibility::Visible);
		World->GetTimerManager().SetTimer(HiddenTimerHandle, HiddenFunc, Time, false);
	}
}

void UShooterOverlay::SetClipAmmoAmount(FText InText)
{
	if (ClipAmmoAmount) {
		ClipAmmoAmount->SetText(InText);
	}
}

void UShooterOverlay::SetCarriedAmmoAmount(FText InText)
{
	if (CarriedAmmoAmount) {
		CarriedAmmoAmount->SetText(InText);
	}	
}

void UShooterOverlay::SetAmmoHintVisible(ESlateVisibility bVisible)
{
	if (ClipAmmoAmount) {
		ClipAmmoAmount->SetVisibility(bVisible);
	}
	if (CarriedAmmoAmount) {
		CarriedAmmoAmount->SetVisibility(bVisible);
	}

	if (AmmoImage) {
		AmmoImage->SetVisibility(bVisible);
	}
}

void UShooterOverlay::SetTimeRemain(FText InText)
{
	if (TimeRemain) {
		TimeRemain->SetText(InText);
	}
}

void UShooterOverlay::SetGrenadeAmount(FText InText)
{
	if (GrenadeAmount) {
		GrenadeAmount->SetText(InText);
	}
}

void UShooterOverlay::SetCurWeaponType(EWeaponTypes NewWeaponType)
{
	if (WeaponType != NewWeaponType) {
		WeaponType = NewWeaponType;
		NotifyUpdateUIInfo(WeaponType);
	}
}
