// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "Components/Image.h"
#include "Components/TextBlock.h"
#include "Feisgo/ShooterTypes/WeaponTypes.h"
#include "ShooterOverlay.generated.h"

/**
 * 
 */
UCLASS()
class FEISGO_API UShooterOverlay : public UUserWidget
{
	GENERATED_BODY()
public:
	void SetHealthText(FText InText);

	void SetHealthTextVisible(ESlateVisibility Visible);

	void SetShieldText(FText InText);
	
	void SetKillAmount(FText InText);

	void FadeKillAmount(float Time);

	void SetClipAmmoAmount(FText InText);

	void SetCarriedAmmoAmount(FText InText);
	
	void SetAmmoHintVisible(ESlateVisibility Visible);

	void SetTimeRemain(FText InText);

	void SetGrenadeAmount(FText InText);

	void SetCurWeaponType(EWeaponTypes NewWeaponType); 

	UFUNCTION(BlueprintImplementableEvent)
	void NotifyUpdateUIInfo(EWeaponTypes NewWeaponType);
protected:
	FTimerHandle HiddenTimerHandle;
	
	UPROPERTY(meta=(BindWidght))
	UTextBlock* HealthText;

	UPROPERTY(meta=(BindWidght))
	UTextBlock* ShieldText;
	
	UPROPERTY(meta=(BindWidght))
	UTextBlock* KillAmount;

	UPROPERTY(meta=(BindWidght))
	UTextBlock* ClipAmmoAmount;

	UPROPERTY(meta=(BindWidght))
	UTextBlock* CarriedAmmoAmount;

	UPROPERTY(meta=(BindWidght), BlueprintReadWrite)
	UImage* AmmoImage;

	UPROPERTY(meta=(BindWidght))
	UTextBlock* TimeRemain;

	UPROPERTY(meta=(BindWidght))
	UTextBlock* GrenadeAmount;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	EWeaponTypes WeaponType = EWeaponTypes::EWT_MAX;
};
